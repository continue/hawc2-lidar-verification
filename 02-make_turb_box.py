# -*- coding: utf-8 -*-
"""
@author: espa
"""

import numpy as np
from _functions import df_to_h2turb, read_windsimu

# %% Generates turbulence with constant values for u, v, w: 
    
Nx, Ny, Nz = 1024, 32, 32
Time = 200

# Turbulence box 1 constant: 
wsp_u = 11.0
wsp_v = 3.0
wsp_w = 2.0
prefix = 'nrel_5mw_wsp11.0_' + 'turb_cte'

Lx = (Time * wsp_u) /Nx
Ly = 130 / (Ny-1)
Lz = 130 / (Nz-1)

u = np.ones((Nx, Ny, Nz)) * wsp_u
v = np.ones((Nx, Ny, Nz)) * wsp_v
w = np.ones((Nx, Ny, Ny)) * wsp_w
wsp = np.sqrt(np.mean(wsp_u)**2 + wsp_v**2 + wsp_w**2)

path = '.\\nrel_5mw\\turb\\'
comp = [u, v, w]

# Save turbulence box in binary file for HAWC2:
df_to_h2turb(prefix, path, comp)

# Reads the binary file to check correct creation:
check = True
if check: 
    u1, v1, w1 = read_windsimu(path, prefix,  Nx, Ny, Nz)
