# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 13:18:03 2023

@author: espa
"""

import _functions_wgh as fwf
  
# Continuous wave lidar: 
F = 100  # focus length along the beam
gamma = 1565 * 10**(-9)
alpha = 28 * 10**(-3)
dr_value = 50
half_width = 6  # half-width of the integration value [multiples of zR]

fwf.plot_cw_weigh_func(F, 100, gamma, alpha, dr_value, half_width)

# Pulsed lidar:
deltaP = 38.4
deltaL = 24.75
half_width = 2.02  # half-width of the integration value, equal to dv in HAWC2
no_int = 100
dr_val = 50

dr = fwf.plot_pulsed_weigh_func(F, deltaP, deltaL, no_int, dr_val, half_width)


# %% Integration Volume:
    
half_width = dr_val / deltaL  # half-width of the integration value, equal to dv in HAWC2

for N in range(4):
    fwf.plot_pulsed_weigh_integ(F, deltaP, deltaL, no_int, dr_val,
                               half_width,N+1)
