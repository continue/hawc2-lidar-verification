"""Plot velocities and PSDs for nacelle and hub lidars.
"""
import matplotlib.pyplot as plt
import numpy as np
from wetb.hawc2.Hawc2io import ReadHawc2

from _functions import check_path
from _values import kw_nrel5mw


FNAME_NEW = 'nrel5mw_wsp_11.0_positive_flex_tower'
FNAME_129 = 'nrel5mw_wsp_11.0_positive_flex_tower_v12'
SAVEFIG = True  # save figure ?

# load results
h2_results = []
for fname in [FNAME_NEW, FNAME_129]:
    hawc2_path = kw_nrel5mw['path_res'] + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    h2_results.append(read_h2.ReadGtsdf())
h2_res_new, h2_res_129 = h2_results

# beam index, starting from zero
beam_no = 4  # beam 5 in htc file is directly into the wind

t = h2_res_new[:, 0]  # Time vector

# isolate the data
LOS_hub_new = h2_res_new[:, kw_nrel5mw['idx_hub_lidar'] + 8*beam_no + 3]
LOS_nl_new = h2_res_new[:, kw_nrel5mw['idx_nacelle_lidar'] + 5*beam_no]
LOS_nl_129 = h2_res_129[:, kw_nrel5mw['idx_nacelle_lidar'] + 5*beam_no]
tower_vel_new = h2_res_new[:, kw_nrel5mw['idx_hub_vel'] + 1]

if not np.all(np.isclose(LOS_hub_new, LOS_nl_new)):  # for Beam 4, hub and nacelle should be same
    print('!!! HUB LIDAR AND NACELLE LIDAR NOT MATCHING FOR 13.1 !!!')

# ------------------ PLOT 1: Velocity time series ------------------

fig, ax1 = plt.subplots(figsize=(12, 6))
ax1.set_xlabel('Time [s]', fontsize=12, fontweight='bold')
ax1.set_ylabel('LOS measurement [m/s]', color='black', fontsize=12, fontweight='bold')
ax1.plot(t, LOS_hub_new, linestyle = '--', marker='o', mfc='none', markevery=60, color='black', label='Hub lidar: 13.1')
ax1.plot(t, LOS_nl_new, linestyle = '-.', marker='x', markevery=(30, 60), color='black', label='Nacelle lidar lidar: 13.1')
ax1.plot(t, LOS_nl_129, color='black', label='Nacelle lidar lidar: 12.9')
ax1.tick_params(axis='y', labelcolor='black')

# Create a second y-axis on the right side
ax2 = ax1.twinx()
ax2.set_ylabel('Hub global velocity, Vy [m/s]', color='blue', fontsize=12, fontweight='bold')
ax2.plot(t, tower_vel_new, 'blue', label='Normalized Hub Velocity Vy')
ax2.tick_params(axis='y', labelcolor='blue')

plt.title("Normalized LOS wind speed and hub global velocity (Vy)", fontsize=16, fontweight='bold')
plt.xlim([100, 120])
plt.grid(True)
plt.tight_layout()

# Create a combined legend for both axes
lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax1.legend(lines + lines2, labels + labels2, loc='upper center', 
            bbox_to_anchor=(0.5, -0.13), fontsize=13,
            fancybox=True, shadow=True, ncol=4)

# Adjust the layout to make room for the legend
plt.subplots_adjust(bottom=0.20)

# save fig if requested
if SAVEFIG:
    fname = 'comparison_los_hub_vel_v2'
    path = '.\\figures\\07_relative_motion\\'
    check_path(path)
    
    plot_name = path + fname + '.png'
    plt.savefig(plot_name, dpi=150)

plt.show()