# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 13:18:03 2023

@author: espa
"""

import numpy as np
from scipy import special
import _functions as func
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

# %% Function to calculate zR:

def calculate_zR(gamma, factor, fd, alpha):
    return (gamma * factor * fd**2) / (np.pi * alpha**2)


# %% Function to calculate weights:

def calculate_weights(zR, dr_int):
    return 1/np.pi * zR / (zR**2 + dr_int**2)


# %% Function to plot CW weighting function:

def plot_cw_weigh_func(fd, no_int, gamma, alpha, dr_val, half_width, save=True):
    
    zR = calculate_zR(gamma, 1.0, fd, alpha)
    
    dr = np.linspace(-dr_val, dr_val, 201)  # r - F
    wc = calculate_weights(zR, dr)
    
    dr_int = np.linspace(-half_width*zR, half_width*zR, no_int)
    wc_int = calculate_weights(zR, dr_int)
    
    #Subplot 1: Nacelle Lidar
    fig1, axs = plt.subplots(nrows = 1, ncols = 1, figsize=(10, 5))
    axs.plot(dr, wc, color='black', label='base')

    factors_and_colors = [(0.8, 'blue'), (1.5, 'red')]
    
    for factor, color in factors_and_colors:
        #zR = calculate_zR(gamma, factor * fd, alpha)
        zR = calculate_zR(gamma, factor, fd, alpha)
        dr_int = np.linspace(-half_width*zR, half_width*zR, no_int)
        wc_int = calculate_weights(zR, dr_int)
        axs.plot(dr_int, wc_int, color=color, label=f'{factor} $f_d$')

    ylim = axs.get_ylim()
    axs.set_xlim([-40, 40])
    axs.set_ylim([0, ylim[1]])
    axs.set_xlabel('s [m]', weight='bold')
    axs.set_ylabel('Weighting Function [1/m]', weight='bold')
    axs.set_title('Nacelle Lidar \n Weighting function ($f_d$ = 100 [m])', weight='bold')
    axs.legend()
    axs.grid(True)
    
    if save:
        fname = 'cw_lidar_wgh_func'
        path = '.\\figures\\05_weightening_functions\\'
        func.check_path(path)
        
        plot_name = path + fname + '.png'
        plt.savefig(plot_name, dpi=150)

    plt.show()

    return


# %% Function to calculate rp: 

def calculate_rp(deltaL):
    return deltaL / (2 * np.sqrt(np.log(2)))


# %% Function to calculate pulsed weighting function:
    
def calculate_wf_pulsed(dr, deltaP, rp):
    return (1 / (2* deltaP)) * (special.erf(((dr + (deltaP/2)) / rp)) - special.erf(((dr - (deltaP/2)) / rp)))


# %% Function to plot pulsed weighting function:

def plot_pulsed_weigh_func(F, deltaP, deltaL, no_int, dr_val, half_width,
                           save=True):
    
    dr = np.linspace(-dr_val, dr_val, no_int)
    rp = calculate_rp(deltaL)
    half_width_1 = half_width * deltaL
    dr_base = np.linspace(-half_width_1, half_width_1, no_int)
    WF_pulsed_base = calculate_wf_pulsed(dr_base, deltaP, rp)

    cases = [
        (deltaL, deltaP, 'base', 'black', 'dummy'),
        (deltaL, deltaP, '2$f_d$', 'yellowgreen', ':'),
        (2*deltaL, deltaP, '2$\Delta l$', 'red', 'dummy'),
        (24.75, 2*deltaP, '2$\Delta p$', 'blue', 'dummy'),
    ]
    
    fig1, axs = plt.subplots(nrows = 1, ncols = 1, figsize=(10, 5))

    for deltaL_case, deltaP_case, label, color, linestyle in cases:
        rp = calculate_rp(deltaL_case)
        half_width_case = half_width * deltaL_case
        dr_case = np.linspace(-half_width_case, half_width_case, no_int)
        WF_pulsed_case = calculate_wf_pulsed(dr_case, deltaP_case, rp)
        if linestyle == 'dummy':
            axs.plot(dr, WF_pulsed_case, label=label, color=color)
        else:
            axs.plot(dr, WF_pulsed_base, label=label,
                     linestyle=linestyle, color=color)

    ylim = axs.get_ylim()
    axs.set_ylim([0, ylim[1]])
    axs.set_xlabel('s [m]', weight='bold')
    axs.set_ylabel('Weighting Function [1/m]', weight='bold')
    axs.set_xlim(-dr_val, dr_val)
    axs.set_title('Hub Lidar \n Weighting function ($f_d$ = 100 [m])', weight='bold')
    axs.legend()
    axs.grid(True)
    
    if save:
        fname = 'pulsed_lidar_wgh_func'
        path = '.\\figures\\05_weightening_functions\\'
        func.check_path(path)
        
        plot_name = path + fname + '.png'
        plt.savefig(plot_name, dpi=150)

    plt.show()

    return dr


# %%


def plot_pulsed_weigh_integ(F, deltaP, deltaL, no_int, dr_val,
                               half_width, N, save=True):
    
    Nu = 2*N + 1
    dr = np.linspace(-dr_val, dr_val, no_int)
    rp = calculate_rp(deltaL)
    half_width_1 = half_width * deltaL
    dr_base = np.linspace(-half_width_1, half_width_1, no_int)
    WF_pulsed_base = calculate_wf_pulsed(dr_base, deltaP, rp)
    
    fig1, axs = plt.subplots(nrows = 1, ncols = 1, figsize=(10, 5))
    wf_line, = axs.plot(dr, WF_pulsed_base, color='blue', linewidth=2)
    wf_fill = axs.fill_between(dr, 0, WF_pulsed_base, color='blue', alpha=0.3)
    
    # Add points at the vertices of the trapezoid
    x_values = np.linspace(-dr_val, dr_val, Nu)
    y_values = np.interp(x_values, dr, WF_pulsed_base)
    int_points = plt.scatter(x_values, y_values, color='orange',
                             zorder=5, s=100) 
    
    # Define the trapezoids
    trapezoids = [np.array([[x_values[i], 0], [x_values[i], y_values[i]], [x_values[i+1], y_values[i+1]], [x_values[i+1], 0]]) for i in range(len(x_values)-1)]

    # Plot the trapezoids and calculate the area
    total_trapezoid_area = 0
    for trapezoid in trapezoids:
        trap_fill = axs.fill(trapezoid[:, 0], trapezoid[:, 1],
                             color='orange', alpha=0.5)
        base = trapezoid[2, 0] - trapezoid[0, 0]
        height_avg = (trapezoid[1, 1] + trapezoid[2, 1]) / 2
        total_trapezoid_area += base * height_avg

    total_trapezoid_area = round(total_trapezoid_area, 2)
    
    ylim = axs.get_ylim()
    axs.set_ylim([0, ylim[1]])
    axs.set_xlabel('(r - F) [m]', weight='bold')
    axs.set_xlim(-dr_val-5, dr_val+5)
    axs.set_ylabel('Weighting Function [1/m]', weight='bold')
    axs.set_title(f'Integration volume for hub-lidar for case N={N}',
                  weight='bold')
    
    # Create legend
    legend_labels = ['Weighting Function',
                     'Integration Points',
                     'Weighted area = 1.0',
                     f'Trapezoids Area = {total_trapezoid_area}']
    legend_elements = [wf_line, int_points,
                       mpatches.Patch(facecolor='blue', alpha=0.3),
                       mpatches.Patch(facecolor='orange', alpha=0.5)]
    axs.legend(legend_elements, legend_labels)

    axs.grid(True)
    
    if save:
        fname = f'hub_lidar_wgh_int_N_{N}'
        path = '.\\figures\\05_weightening_functions\\'
        func.check_path(path)
        
        plot_name = path + fname + '.png'
        plt.savefig(plot_name, dpi=150)

    plt.show()

    return

