# -*- coding: utf-8 -*-
"""
Created on Mon May 15 12:16:53 2023

@author: espa
"""

import pandas as pd
import numpy as np
from _values import kw_nrel5mw, hub_lidar, nacelle_lidar
import _functions as fr

# %% Read all cases and saved result simulations in a dictionary.

# Cases: 
fnames = [
    'nrel5mw_wsp_11.0_positive',
    'nrel5mw_wsp_11.0_negative',
    'nrel5mw_wsp_11.0_yaw_90',
    'nrel5mw_wsp_11.0_yaw_270',
    'nrel5mw_wsp_11.0_shear_0.2',
    'nrel5mw_wsp_11.0_turb_cte',
    'nrel5mw_wsp_11.0_roll_10',
    'nrel5mw_wsp_11.0_tilt_10',
    'nrel5mw_wsp_11.0_yaw_10',
    'nrel5mw_wsp_11.0_roll_10_v12',
    'nrel5mw_wsp_11.0_tilt_10_v12',
    'nrel5mw_wsp_11.0_yaw_10_v12',
    'nrel5mw_wsp_11.0_turb_s927_v13.1'
]

cases = {}
beam_no = 7

for fname in fnames:
    print(fname)
    if 'v12' in fname:
        cases[fname] = fr.save_df_res(fname, beam_no, version='v12', **kw_nrel5mw)
        print(f'Using HAWC2 v12 function for file {fname}.')
    elif 's927' in fname:
        # json file will be used in script 05:
        cases[fname] = fr.save_df_res(fname, beam_no, version='v13', save=True, **kw_nrel5mw)
    else: 
        cases[fname] = fr.save_df_res(fname, beam_no, version='v13', **kw_nrel5mw)
        print(f'Using HAWC2 v13 function for file {fname}.')
        
# %% Initializing cases: 

limit = [20, 40]
axis = 'time'
xo = nacelle_lidar['x0']  # Initial offset Nacelle lidar
yo = nacelle_lidar['y0']  # Initial offset Nacelle lidar
zo = nacelle_lidar['z0']  # Initial offset Nacelle lidar

# To plot Global positions and line-of-sight wind speeds:
wsp, n_vector = fr.plot_wsp_and_pos(cases, axis, limit, hub_lidar, 
                                    kw_nrel5mw, wsp=True, pos=True)

# %% To assert values between HAWC2 results and theoreticals:
tol_pos = 0.25  # Tolerance for Global positions in m.
tol_wsp = 0.10  # Tolerance for the weighted LOS wind speed in m/s.
output_file = 'Assert_results.txt'  # Text file for the assertion verification.
asssert_state = fr.assert_pos_and_wsp(cases, wsp, n_vector,
                                   tol_pos, tol_wsp, output_file)

# plot time series comparison of hub lidar, nacelle lidar, and free wind w/turbulence
fr.plot_nl_hl_fw(kw_nrel5mw)

# %% Table 3.5 Report - Nacelle lidar Global positions differences v13.1 and v12.9

rotations = ['Tilt', 'Yaw', 'Roll']
deltas = ['x', 'y', 'z']
beam_columns = ['Beam 1', 'Beam 2', 'Beam 5']

# Create an empty DataFrame with the desired columns
columns = ['Rotation', *beam_columns]
df = pd.DataFrame(columns=columns)

# Calculate and add values to the DataFrame
for rotation in rotations:
    
    if rotation == 'Tilt':
        case_name = 'nrel5mw_wsp_11.0_tilt_10'
    elif rotation == 'Yaw':
        case_name = 'nrel5mw_wsp_11.0_yaw_10'
    else:
        case_name = 'nrel5mw_wsp_11.0_roll_10'
        
    for delta in deltas:
        row = [f"{rotation} Delta_{delta}"]
        
        for beam in beam_columns:
            
            if beam == 'Beam 1':
                b = 0
            elif beam == 'Beam 2':
                b = 1
            else:
                b = 4
            
            value = np.mean(cases[case_name][b][f'nac_{delta}'] - cases[f'{case_name}_v12'][b][f'nac_{delta}'])
            value = round(value, 3)
            row.append(value)
        df.loc[len(df)] = row

# Convert the DataFrame to LaTeX format
latex_table = df.to_latex(index=False)

# Print or save the LaTeX table as needed
print(latex_table)
