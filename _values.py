# -*- coding: utf-8 -*-
"""
Dictionaries with variables and parameters for:
    - Wind Turbine model: NREL 5 MW
    - HAWC2 Simulation parameters and result analysis

@author: espa
"""

import os
import numpy as np

# To locate Mann generator executable:
CURRENT_DIR = os.path.dirname(__file__)
PROJECT_ROOT = os.path.abspath(os.path.join(CURRENT_DIR,
                                            os.pardir))

# NREL 5MW parameters
kw_nrel5mw = dict(tilt=5*np.pi/180,  # tilt [rad]
                  tilt_statment=False, 
                  shaft_length=-5.0191,  # shaft length [m]
                  D=126,  # Rotor Diameter [m]
                  z_hub=-89.5624,  # Hub Height
                  d_rotor=126,  # rotor diameter [m]
                  tb_wid=130,  # Width of TurbBox [m]
                  tb_ht=130,  # Height of TurbBox [m]
                  alpha=0.2,  # For Shear Profile
                  name='nrel_5mw',  # Wind turbine model
                  idx_time=0,  # HAWC2 Index Time, 1
                  idx_omega=9,  # HACW2 index for Omega, 1
                  idx_hub_pos=60,  # HAWC2 index for Hub pos, 3: x, y, z
                  idx_hub_vel=63, # HAWC2 index for Hub velocity, 3: x, y, z
                  idx_tower_base=17,  # HAWC2 index for Tower base moment, 3: Mx, My, Mz
                  # Nacelle Lidar: 
                  # Line of sight, Doppler Spectrum, x, y, z
                  # Total of 7 beams in study
                  idx_nacelle_lidar=72,  # HAWC2 index Nacelle lidar, 5
                  # Hub Mounted Lidar:
                  idx_hub_lidar=107,  # HAWC2 Channel for first HMLidar, 8
                  idx_sens_hl=184,
                  # x, y, z positions 1 to 3
                  # Weighted LOS velocity
                  # Nominal LOS velocity
                  # Free Wind:
                  idx_free_wind=163,  # HAWC2 index for free Wind, 3: x, y, z
                  # Path HAWC2 res to read for post-processing:
                  path_res='.\\nrel_5mw\\res\\',
                  )

# Hub Mounted Lidar Parameters for HAWC2
hub_lidar = dict(theta=[15, 15, 15, 15, 0, 90, 90],  # Half-cone angle lidar beam
                 psi=[0, 90, 180, 270, 0, 0, 180],  # Azim. angle [deg]
                 ranges=[100, 100, 100, 100, 100, 50, 50],  # Ranges lidar beam
                 # For roll, yaw, tilt cases, change in one position:
                 theta_p=[15, 15, 15, 15, 0, 0, 90],  # Half-cone angle lidar beam  
                 ranges_p=[100, 100, 100, 100, 100, 0, 50],  # Ranges lidar beam 
                 D=126,  # Rotor Diameter
                 # Percentage of rotor area, where 1 = 100%:
                 Rtarget=0.969235,
                 #tilt_ext=Range*np.tan(tilt),  # Percentage based on tilt
                 htc_begin=' aero nose_cone_lidar ',  # HAWC2 output line
                 # Lidar parameters:
                 deltaP=38.4,  # range-gate length
                 deltaL=24.75,  # lidar beam full width at half-Maximum
                 dv=1.81,  # Half-width of the integration volume
                 N_int=100,  # Number of integration points in dv.
                 config_path='.\\files\\',  # path where files are saved.
                 config_file='Lidar_beams_hawc2.csv',  # File name
                 # File with Lidar Configurations that want to be study:
                 config_userfile='Lidar_config_user.xlsx'  # Given by the user
                 )

# Nacelle Lidar Parameters for HAWC2
nacelle_lidar = dict(x0=0,  # Mounting distance from rotor center in x [m]
                     y0=0,  # Mounting distance from rotor center in y [m]
                     z0=0,  # Mounting distance from rotor center in z [m]
                     # Half-cone angle of beam [deg]:
                     theta=[15, 15, 15, 15, 0, 90, 90],
                     psi=[0, 90, 180, 270, 0, 0, 180],  # Azim. angle [deg]
                     # Focus length measured from rotor [m]
                     focus_length=[100, 100, 100, 100, 100, 50, 50],
                     Rayleigth_length=6.5,  # Rayleight length (Gamma) [m]
                     dv=6,  # Half-width integration interval over probe volume
                     N_int=100,  # Number of integrations points
                     beam_n=[1, 2, 3, 4, 5, 6, 7],  # Beam Identifier Number
                     )

    
Nx, Ny, Nz = 1024, 32, 32

kw_turbgen = dict(Nx=int(Nx),  # Number of grid points in the x direction
                  Ny=int(Ny),  # Number of grid points in the y direction
                  Nz=int(Nz),  # Number of grid points in the z direction
                  x0=0,  # Initial x coordinate System HAWC2
                  y0=0,  # Initial y coordinate System HAWC2
                  z0=-89.5624,
                  hub_height=kw_nrel5mw['z_hub'],  # Initial z coordinate System HAWC2
                  Time=40,  # Time simulation HAWC2
                  tstart=0.01,  # Time Start
                  deltat=0.01,  # Delta t for HAWC2 Sim
                  alpha=0.2,  # For Shear Profile
                  # Width and Height turbulence box [m]:
                  U=11.0,
                  tb_wid=kw_nrel5mw['tb_wid'],
                  tb_ht=kw_nrel5mw['tb_ht'],
                  # Length of the grid box in the y, z direction [m]
                  Ly=kw_nrel5mw['tb_wid'] / (Ny-1),
                  Lz=kw_nrel5mw['tb_ht'] / (Nz-1),
                  L=33.6,  # Mann Model length scale parameter
                  gamma=3.9,  # Mann Model Anasotropy parameter
                  ae=0.2987056673,  # Man model alphaepsilong parameter
                  HighFreqComp=1,  # If 1, HighFreqComp = True, if 0 = False.
                  wsps=[11.4],  # Wind Speed [m/s]
                  Seeds=927,  # Seed Number
                  I_ref=0.16,  # Turbulence intensity
                  turbine='nrel_5mw',  # Wind turbine model
                  # Path to save Turbulence Boxes binary files:
                  path_save='.\\nrel_5mw\\turb\\'
                  )