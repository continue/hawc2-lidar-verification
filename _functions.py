# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 16:01:40 2023

@author: espa
"""

import numpy as np
import pandas as pd
import os
import pickle
import datetime
import matplotlib.pyplot as plt
from wetb.hawc2 import HTCFile
from wetb.hawc2.Hawc2io import ReadHawc2


MANN_BOX_FMT = '<f'  # binary turbulence datatype
_HAWC2_BIN_FMT = '<f'  # HAWC2 binary turbulence datatype


# %% Function that calculates position of Free Wind in HAWC2 Global coord.


def calc_fw_pos(theta, psi, fl, shaft_length, hub_height):
    """
    Calculates the Free wind positions in HAWC2 Global coordinate system for
    the nacelle lidar verification.

    Parameters
    ----------
    theta : list
        Half-cone angle in degrees.
    psi : list
        Azimuthal angle in degrees.
    fl : list
        Focus length in [m].
    shaft_length: float
        Shaft lenght of the wind turbine model in meters.
    hub_height: float
        End height of the hub in meters. Noticed it is not equal to the hub
        height of the wind turbine model. 

    Returns
    -------
    hawc2_coor : array
        HAWC2 Global Coordinates for free wind for the inputs.

    """
    r = np.sin(np.radians(theta))*fl
    x_fw = round(np.sin(np.radians(psi))*r, 4)
    y_fw = round(-(np.cos(np.radians(theta))*fl - shaft_length), 4)
    z_fw = round((-1)*np.cos(np.radians(psi))*r + hub_height, 4)
    hawc2_coor = [x_fw, y_fw, z_fw]
    return hawc2_coor


# %%  Function to generate htc files for verification:


def generate_htc_files(orig_htc, wsp, cases, **kw):
    """
    Calls the function make_htc_sc to generate the htc files for the
    different cases. Deoending on the case, this function add the corresponding
    value to different parameters such as shear, wind field rotation, and
    turbulence files values. 


    Parameters
    ----------
    orig_htc : str
        File name for the master htc file.
    wsp : float
        Wind speed in [m/s].
    cases : dict
        Dictionary with the cases used for the verification report.
    **kw : dict
        Dictionary with the values for the wind turbine model used in the
        study. 

    Returns
    -------
    None.

    """
    
    tstop = 40
    
    for case, case_name in cases.items():
        
        # wind_field rotation:
        if 'yaw_90' in case_name:
            rot = [90, 0, 0]
        elif 'negative' in case_name:
            rot = [180, 0, 0]
        elif 'yaw_270' in case_name:
            rot = [270, 0, 0]
        else:
            rot=[0, 0, 0]
        
        # tilt and roll
        kw['tilt'] = 10 if ('tilt_10' in case_name or 'flexible' in case_name) else 0
        kw['roll'] = 10 if 'roll_10' in case_name else 0
        kw['yaw'] = 10 if 'yaw_10' in case_name else 0

        #To check shear:
        if 'shear' in case_name:
            shear_format = 'power'
        elif 'turb' in case_name:
            shear_format = 'none'
        else:
            shear_format = 'cte'
        
        # turbulence settings
        if 'turb_s927' in case_name:
            turb = True
            tstop = 200 - 0.01
            shear_format = 'none'
            turb_name = 'nrel_5mw_wsp11.4_s927'
        elif 'turb_cte' in case_name:
            turb = True
            shear_format = 'none'
            tstop = 200 - 0.01
            turb_name = 'nrel_5mw_wsp11.0_turb_cte'
        elif 'positive_flex_tower' in case_name:
            kw['tstart'] = 100
            kw['tower_flex'] = True
            tstop = 125
        else:
            turb = False
            turb_name = 'dummy'
            
        # Generate htc files: 
        make_htc_sc(orig_htc, case_name, wsp, rot, tstop, 
                    turb_name, shear_format, turb,
                    **kw)
    return print('\nAll htc files has been generated. ')


# %% Function to generate htc files:


def make_htc_sc(orig_htc, file_name, wsp, rot, tstop, turb_name,
                shear_format='none', turb=False, tstart=0.01,
                tower_flex=False, tilt=0, yaw=0, roll=0,
                **kw):
    """
    Generates htc files for the HAWC2 Sanity Checks.

    Parameters
    ----------
    orig_htc : str
        htc template
    file_name : str
        Name for the case htc, res, log.
    wsp : float
        Wind Speed
    shear_format : str, optional
        Shear format, it can be none or power. The default is 'none'.
    rot : array
        Rotational vector, array size (3,1).

    Raises
    ------
    TypeError
        If the shear profile is not none or power, it will raised an error.

    Returns
    -------
    None.

    """
        
    if turb == False: 
        turb_format = 0,  # No turbulence box
        tint = 0.0
    else: 
        turb_format = 1,  # Mann turbulence box   
        tint = 0.1

    Time = tstop      
    htc = HTCFile(orig_htc)
    tower_shadow=3
    
    if shear_format=='none':
        shear = 0
        alpha = 0
    elif shear_format=='cte':
        shear = 1
        alpha = 0
    elif shear_format=='power':
        shear = 3
        alpha = kw['alpha']
    else:
        raise TypeError('Input must be none, cte or power.')
        
    # set simulation time and file names
    htc.set_time(start=tstart, stop=tstop, step=kw['deltat'])
    htc.set_name(file_name)
    # set tower flexibility and tilt, roll and yaw
    tower_set = [1, 1] if tower_flex else [1, 2]
    htc.new_htc_structure.main_body.timoschenko_input.set = tower_set
    htc.new_htc_structure.orientation.relative__2.body2_eulerang__2 = [tilt, yaw, roll]
    # if flexible tower, add step in wind
    if tower_flex:
            htc.wind.wind_ramp_abs = [tstart + 2, tstart + 3, 0, 1]
    # Wind Block:     
    htc.wind.wsp = wsp  # mean wind speed
    htc.wind.tint = tint  # turbulence intensity
    htc.wind.windfield_rotations = rot
    htc.wind.center_pos0 = [0.0, 0.0, kw['hub_height']]
    htc.wind.shear_format = [shear, alpha]  # Constant
    htc.wind.turb_format = turb_format  # none
    htc.wind.tower_shadow_method = tower_shadow
    if turb == True: 
        if 'mann' in htc.wind:
            del htc.wind.mann
        mann = htc.wind.add_section('mann')
        mann.add_line('create_turb_parameters',
                      [kw['L'], kw['ae'], kw['gamma'],
                       kw['Seeds'], kw['HighFreqComp']])
        mann.add_line('filename_u', [f'./turb/{turb_name}_u.bin'])
        mann.add_line('filename_v', [f'./turb/{turb_name}_v.bin'])
        mann.add_line('filename_w', [f'./turb/{turb_name}_w.bin'])
        mann.add_line('box_dim_u', [kw['Nx'], kw['U'] * Time / kw['Nx']])
        mann.add_line('box_dim_v', [kw['Ny'], kw['tb_wid'] / (kw['Ny'] - 1)])
        mann.add_line('box_dim_w', [kw['Nz'], kw['tb_ht'] / (kw['Nz'] - 1)])
        mann.add_line('dont_scale', [1])   
    # Get current date and time
    now = datetime.datetime.now()
    folder_name = f"lidar_{now.strftime('%Y-%m-%d_%H-%M')}\\"
    base_path = ".\\nrel_5mw\\htc\\"
    full_path = os.path.join(base_path, folder_name)
    
    if not os.path.exists(full_path):
        os.makedirs(full_path)
    
    new_htc = full_path + file_name + '.htc'
    # save the new file
    htc.save(new_htc)
    print(f'HTC File has been saved as: {new_htc}.')
    return


# %% Transform DataFrame to binary HAWC2 turbulence box format:


def df_to_h2turb(prefix, folder, comp):
    """
    Function to transform DataFrame to HAWC2 turbulence box format.

    Parameters
    ----------
    folder : str
        Path where to save the turbulence box.
    comp : DataFrame
        DataFrame with the data for the turbulence box

    Returns
    -------
    None.

    """
    # make and save binary files for one component.
    for c, b in zip('uvw', comp):
        print(c)
        bin_path = os.path.join(folder,
                                f'{prefix}_{c}.bin')
        print(bin_path)
        # assert os.path.isfile(bin_path)
        with open(bin_path, 'wb') as bin_fid:
            b.astype(np.dtype(MANN_BOX_FMT)).tofile(bin_fid)
    print('Turbulence box has been saved as {bin_path}. \n')
    return

# %% Function to read HAWC2 turbulence box binary format:


def read_windsimu(path, file_name, Nx, Ny, Nz):
    """
    Reads HAWC2 binary turbulence box format file, and saved it as an array.

    Parameters
    ----------
    path : str
        Path where the binary files is stored.
    file_name : str
        File name for the HAWC2 binary file for the turbulence box.
    Nx, Ny, Nz : int
        Number of points for the turbulence box in the x, y, z direction.
        
    Returns
    -------
    u, v, w : 3D array
        Turbulence box with dimensions Nx, Ny, Nz for the wind component
        u, v, w.

    """
    
    u_file_path = path + file_name + '_u.bin'
    v_file_path = path + file_name + '_v.bin'
    w_file_path = path + file_name + '_w.bin'
    # Read in the turb files
    with open(u_file_path, 'rb') as bin_fid:
        u = np.fromfile(
            bin_fid,
            dtype=MANN_BOX_FMT).reshape((Nx, Ny, Nz)
                                        )
    with open(v_file_path, 'rb') as bin_fid:
        v = np.fromfile(
            bin_fid,
            dtype=MANN_BOX_FMT).reshape((Nx, Ny, Nz)
                                        )
    with open(w_file_path, 'rb') as bin_fid:
        w = np.fromfile(
            bin_fid,
            dtype=MANN_BOX_FMT).reshape((Nx, Ny, Nz)
                                        )
    return u, v, w


# %% Function to save figures from plots:

def save_figure(fname, case, prefix, dpi=150):
    """
    Saves figures from each plot in the default path.

    Parameters
    ----------
    fname : str
        Name of the case for the figures.
    case : int
        Beam number from the case in study.
    prefix : str
        Describe the wind speed component that is being
        plotted.

    Returns
    -------
    None.

    """
    path = '.\\figures\\' + fname + '\\'
    check_path(path)
        
    plot_name = path + prefix + fname + f'_beam_{case}.png'
    plt.savefig(plot_name, dpi=150)
    
    print(f'Plot has been created and saved as {plot_name}. \n')
        
    return



# %% Function to extract values from HAWC2 v13.1 results:


def save_df_res(fname, beam_no, version='v13', save=False, **kw_nrel5mw):
    """
    Saves the values of the HAWC2 results simulations in a DataFrame, for the
    nacelle lidar, hub lidar and free wind.
    
    It manage both versions v12.9 and v.13.1 from HAWC2, since these version
    provide different output and command for the hub_lidar, and must be
    handle differently. 

    Parameters
    ----------
    fname : str
        File name of the HAWC2 results.
    beam_no : int
        Number of beams per lidar in the study. Must be the same number for
        nacelle and hub-mounted lidar.
    version : str
        Provides the version of HAWC2 (v13 for HAWC2 v13.06 or above, and
                                       v12 for HAWC2 v12.9)
    **kw_nrel5mw : dict
        Dictionary with the values for the wind turbine model.

    Returns
    -------
    res : dict
        Dictionary, where each key is the number of the case (beam case) in
        study. For each beam case, a DataFrame is generated, with the values:
            Time [s], shaft rotation [deg], 
            hub_x [m], hub_y [m], hub_z [m],
            nac_LOS [m/s], nac_dop_var [x], nac_x [m], nac_y [m], nac_z [m],
            hl_x [m], hl_y [m], hl_z [m], 
            hl_LOS_wgh [m/s], hl_LOS_nom [m/s], 
            hl_fw_x [m/s], hl_fw_y [m/s], hl_fw_z [m/s], 
            Vx_fw [m/s], Vy_fw [m/s], Vz_fw [m/s]. 


    """   
    hawc2_path = kw_nrel5mw['path_res'] + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()
    # Creating the DataFrame:
    res = dict()
    for i in range(beam_no): 
        time = hawc2_res[:, 0]
        shaft_rot = hawc2_res[:, 1]
                
        # Hub position:
        idx = kw_nrel5mw['idx_hub_pos']
        hub_x = hawc2_res[:, idx]
        hub_y = hawc2_res[:, idx + 1]
        hub_z = hawc2_res[:, idx + 2]
        
        # Hub velocity: 
        idx = kw_nrel5mw['idx_hub_vel']
        hub_vel_x = hawc2_res[:, idx]
        hub_vel_y = hawc2_res[:, idx + 1]
        hub_vel_z = hawc2_res[:, idx + 2]
        
        #Nacelle Lidar:
        idx = kw_nrel5mw['idx_nacelle_lidar'] + i*5
        nac_LOS_wsp = hawc2_res[:, idx]
        nac_ds_var = hawc2_res[:, idx + 1]
        nac_x = hawc2_res[:, idx + 2]
        nac_y = hawc2_res[:, idx + 3]
        nac_z = hawc2_res[:, idx + 4]
        
        if version == 'v13':

            # Free Wind:
            idx = kw_nrel5mw['idx_free_wind'] + i*3
            Vx_fw = hawc2_res[:, idx]
            Vy_fw = hawc2_res[:, idx + 1]
            Vz_fw = hawc2_res[:, idx + 2]  
            
            # Hub-Mounted Lidar:
            idx = kw_nrel5mw['idx_hub_lidar'] + i*8
            hl_x = hawc2_res[:, idx]
            hl_y = hawc2_res[:, idx + 1]
            hl_z = hawc2_res[:, idx + 2]
            hl_LOS_wgh = hawc2_res[:, idx +3]
            hl_LOS_nom = hawc2_res[:, idx + 4]
            hl_fw_x = hawc2_res[:, idx + 5]
            hl_fw_y = hawc2_res[:, idx + 6]
            hl_fw_z = hawc2_res[:, idx + 7]
            
            # Stacking results:
            res[i] = np.column_stack((time, shaft_rot,
                                  hub_x, hub_y, hub_z,
                                  hub_vel_x, hub_vel_y, hub_vel_z,
                                  nac_LOS_wsp, nac_ds_var, nac_x, nac_y, nac_z,
                                  hl_x, hl_y, hl_z, hl_LOS_nom, hl_LOS_wgh,
                                  hl_fw_x, hl_fw_y, hl_fw_z,
                                  Vx_fw, Vy_fw, Vz_fw))
            
            # Columns name for DataFrame:
            column_labels = ['Time', 'shaft_rot',
                             'hub_x', 'hub_y', 'hub_z',
                             'hub_vel_x', 'hub_vel_y', 'hub_vel_z',
                             'nac_LOS', 'nac_Dop_var', 'nac_x', 'nac_y', 'nac_z',
                             'hl_x', 'hl_y', 'hl_z', 'hl_LOS_wgh', 'hl_LOS_nom',
                             'hl_fw_x', 'hl_fw_y', 'hl_fw_z',
                             'Vx_fw', 'Vy_fw', 'Vz_fw']
            
        elif version =='v12':
            
            # Free Wind:
            idx = (kw_nrel5mw['idx_free_wind']-56) + i*3  # should be 107 for v12
            Vx_fw = hawc2_res[:, idx]
            Vy_fw = hawc2_res[:, idx + 1]
            Vz_fw = hawc2_res[:, idx + 2]  
            
            # Place dummy values (nacelle lidar) instead of hub-mounted lidar (assumed in later script):
            idx = kw_nrel5mw['idx_nacelle_lidar'] + i*5
            hl_x = hawc2_res[:, idx + 2]
            hl_y = hawc2_res[:, idx + 3]
            hl_z = hawc2_res[:, idx + 4]
            hl_LOS_wgh = hawc2_res[:, idx]
            hl_LOS_nom = hawc2_res[:, idx]    
            
            # Stacking results:
            res[i] = np.column_stack((time, shaft_rot,
                              hub_x, hub_y, hub_z,
                              hub_vel_x, hub_vel_y, hub_vel_z,
                              nac_LOS_wsp, nac_ds_var, nac_x, nac_y, nac_z,
                              hl_x, hl_y, hl_z, hl_LOS_nom, hl_LOS_wgh,
                              Vx_fw, Vy_fw, Vz_fw))
            
            # Columns name for DataFrame:
            column_labels = ['Time', 'shaft_rot',
                         'hub_x', 'hub_y', 'hub_z',
                         'hub_vel_x', 'hub_vel_y', 'hub_vel_z',
                         'nac_LOS', 'nac_Dop_var', 'nac_x', 'nac_y', 'nac_z',
                         'hl_x', 'hl_y', 'hl_z', 'hl_LOS_wgh', 'hl_LOS_nom',
                         'Vx_fw', 'Vy_fw', 'Vz_fw']
        
        else:
            raise ValueError('No proper HAWC2 version has been selected. \n'
                         'Please, provide version=v13 for HAWC2 versions'
                         ' above v13.0 or version=v12 for HAWC2 v12.9.')
        
        res[i] = pd.DataFrame(res[i], columns=column_labels)

    if save:
        path = '.\\files\\'
        check_path(path)
    
        json_file = path + fname + '.pickle'
        with open(json_file, 'wb') as file:
            pickle.dump(res, file)
        print(f'DataFrame has been saved as: {json_file}. \n')
    
    return res


# %% Save the results from the simulation for the relative motion analysis:


def save_df_res_vrel(fname, beam_no, save=False, **kw_nrel5mw):
    """
    Saves the values of the HAWC2 results simulations in a DataFrame, for the
    nacelle lidar, hub lidar and free wind.

    Parameters
    ----------
    fname : str
        File name of the HAWC2 results.
    beam_no : int
        Number of beams per lidar in the study. Must be the same number for
        nacelle and hub-mounted lidar.
    **kw_nrel5mw : dict
        Dictionary with the values for the wind turbine model.

    Returns
    -------
    res : dict
        Dictionary, where each key is the number of the case (beam case) in
        study. For each beam case, a DataFrame is generated, with the values:
            Time [s], shaft rotation [deg], 
            hub_x [m], hub_y [m], hub_z [m],
            nac_LOS [m/s], nac_dop_var [x], nac_x [m], nac_y [m], nac_z [m],
            hl_x [m], hl_y [m], hl_z [m], 
            hl_LOS_wgh [m/s], hl_LOS_nom [m/s], 
            hl_fw_x [m/s], hl_fw_y [m/s], hl_fw_z [m/s], 
            Vx_fw [m/s], Vy_fw [m/s], Vz_fw [m/s]. 


    """   
    hawc2_path = kw_nrel5mw['path_res'] + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()
    # Creating the DataFrame:
    res = dict()
    for i in range(beam_no): 
        time = hawc2_res[:, 0]
        shaft_rot = hawc2_res[:, 1]
        omega = hawc2_res[:, 9]
        
        # Hub relative velocity:
        idx = kw_nrel5mw['idx_hub_vel']
        hub_vel_x = hawc2_res[:, idx]
        hub_vel_y = hawc2_res[:, idx + 1]
        hub_vel_z = hawc2_res[:, idx + 2]
        
        # Tower-base Moments:
        idx = kw_nrel5mw['idx_tower_base']
        Mx_TB = hawc2_res[:, idx]
        My_TB = hawc2_res[:, idx + 1]
        Mz_TB = hawc2_res[:, idx + 2]
        
        # Hub position:
        idx = kw_nrel5mw['idx_hub_pos']
        hub_x = hawc2_res[:, idx]
        hub_y = hawc2_res[:, idx + 1]
        hub_z = hawc2_res[:, idx + 2]
        
        #Nacelle Lidar:
        idx = kw_nrel5mw['idx_nacelle_lidar'] + i*5
        nac_LOS_wsp = hawc2_res[:, idx]
        nac_ds_var = hawc2_res[:, idx + 1]
        nac_x = hawc2_res[:, idx + 2]
        nac_y = hawc2_res[:, idx + 3]
        nac_z = hawc2_res[:, idx + 4]
        
        # Hub-Mounted Lidar:
        idx = kw_nrel5mw['idx_hub_lidar'] + i*8
        hl_x = hawc2_res[:, idx]
        hl_y = hawc2_res[:, idx + 1]
        hl_z = hawc2_res[:, idx + 2]
        hl_LOS_wgh = hawc2_res[:, idx +3]
        hl_LOS_nom = hawc2_res[:, idx + 4]
        hl_fw_x = hawc2_res[:, idx + 5]
        hl_fw_y = hawc2_res[:, idx + 6]
        hl_fw_z = hawc2_res[:, idx + 7]
        
        # Free Wind:
        idx = kw_nrel5mw['idx_free_wind'] + i*3
        Vx_fw = hawc2_res[:, idx]
        Vy_fw = hawc2_res[:, idx + 1]
        Vz_fw = hawc2_res[:, idx + 2]    
        
        # Stacking results:
        res[i] = np.column_stack((time, shaft_rot, omega,
                              hub_x, hub_y, hub_z,
                              hub_vel_x, hub_vel_y, hub_vel_z,
                              Mx_TB, My_TB, Mz_TB,
                              nac_LOS_wsp, nac_ds_var, nac_x, nac_y, nac_z,
                              hl_x, hl_y, hl_z, hl_LOS_nom, hl_LOS_wgh,
                              hl_fw_x, hl_fw_y, hl_fw_z,
                              Vx_fw, Vy_fw, Vz_fw))
        
        # Columns name for DataFrame:
        column_labels = ['Time', 'shaft_rot', 'omega',
                         'hub_x', 'hub_y', 'hub_z',
                         'hub_vel_x', 'hub_vel_y', 'hub_vel_z',
                         'Mx_TB', 'My_TB', 'Mz_TB',
                         'nac_LOS', 'nac_Dop_var', 'nac_x', 'nac_y', 'nac_z',
                         'hl_x', 'hl_y', 'hl_z', 'hl_LOS_wgh', 'hl_LOS_nom',
                         'hl_fw_x', 'hl_fw_y', 'hl_fw_z',
                         'Vx_fw', 'Vy_fw', 'Vz_fw']
        
        res[i] = pd.DataFrame(res[i], columns=column_labels)
        
    if save: 
        path = '.\\files\\'
        check_path(path)
    
        json_file = path + fname + '.pickle'
        with open(json_file, 'wb') as file:
            pickle.dump(res, file)
        print(f'DataFrame has been saved as: {json_file}. \n')

    return res

# %% Function to check if path exist, if not create the path:


def check_path(path):
    """
    Function to check if path exist, if not creates the path.

    Parameters
    ----------
    path : str
        Path location.

    Returns
    -------
    None.

    """
    if not os.path.exists(path):
        os.makedirs(path)
        print(f'Folder {path} created.')
    else: 
        print(f'Folder {path} already exists. \nFiles will be overwritten.')
    return
    


# %% Function to split title for plots:


def split_title(case_name):
    """
    Function that provide titles for plots depending on the study cases.

    Parameters
    ----------
    case_name : str
        Case name for the study case, based on the res file name from HAWC2. 

    Returns
    -------
    title : str
        Title for the plot based on the study case. 

    """
    

    if 'positive' in case_name: 
        title = 'constant positive longitudinal wind speed'   
        if 'flex_tower' in case_name:
            title = 'constant positive longitudinal wind speed\nwith flexible tower' 
    elif 'negative' in case_name:
        title = 'constant negative longitudinal wind speed'
    elif 'yaw_90' in case_name:
        title = 'lateral negative (yaw=90\u00b0) wind speed'
    elif 'yaw_270' in case_name:
        title = 'lateral positive (yaw=270\u00b0) wind speed'
    elif 'shear' in case_name:
        title = 'power shear profile with constant wind speed'
    elif 'turb_cte' in case_name:
        title = 'generated turbulence'
    elif 'v13' in case_name:
        title = 'Mann turbulence box V=11.4 [m/s] in HAWC2 v13.1'
    elif 'v12' in case_name:
        title = 'Mann turbulence box V=11.4 [m/s] in HAWC2 v12.9'
    elif 'roll_10' in case_name:
        title = 'constant positive longitudinal wind speed with (roll=10\u00b0)'
    elif 'tilt_10' in case_name:
        title = 'constant positive longitudinal wind speed with (tilt=10\u00b0)'
    elif 'yaw_10' in case_name:
        title = 'constant positive longitudinal wind speed with (yaw=10\u00b0)'
    else:
        raise TypeError('The case name was not found. \n')
        title = ''

    print(f'The title provided for {case_name} is {title}. \n')
    return title


# %% Function to make tables with the positiions: 


def make_table_pos(cases, fname, beam_no):
    
    table = dict(hub_x = cases[fname][beam_no]['hub_x'],
                 hub_y = cases[fname][beam_no]['hub_y'],
                 hub_z = cases[fname][beam_no]['hub_z'],
                 nac_x = cases[fname][beam_no]['nac_x'],
                 nac_y = cases[fname][beam_no]['nac_y'],
                 nac_z = cases[fname][beam_no]['nac_z'],
                 hl_x = cases[fname][beam_no]['hl_x'],
                 hl_y = cases[fname][beam_no]['hl_y'],
                 hl_z = cases[fname][beam_no]['hl_z'],
                 nac_VLOS = cases[fname][beam_no]['nac_LOS'],
                 diff_nac_x = cases[fname][beam_no]['hub_x'] - cases[fname][beam_no]['nac_x'],
                 diff_nac_y = cases[fname][beam_no]['hub_y'] - cases[fname][beam_no]['nac_y'],
                 diff_nac_z = cases[fname][beam_no]['hub_z'] - cases[fname][beam_no]['nac_z'],
                 diff_hub_x = cases[fname][beam_no]['hub_x'] - cases[fname][beam_no]['hl_x'],
                 diff_hub_y = cases[fname][beam_no]['hub_y'] - cases[fname][beam_no]['hl_y'],
                 diff_hub_z = cases[fname][beam_no]['hub_z'] - cases[fname][beam_no]['hl_z'])
    
    df_table = pd.DataFrame(table)
    
    return df_table


# %% Function to generate table with mean values positions:


def make_table_avg(table, fnames):
    table_avg = {}  
    for fname in fnames:
        stats = {'Mean': table[fname].mean(),
                 'Min': table[fname].min(),
                 'Max': table[fname].max()}
        table_avg[fname] = pd.DataFrame(stats).transpose()
    return table_avg


# %% Function rotational matrices:


def rot_matrix_lidar(tilt_angle, yaw_angle, roll_angle, lidar):
    """
    Function that calculates the rotational matrices combination for the
    nacelle and hub lidar, based on the tilt, yaw and roll angles in degrees. 

    Parameters
    ----------
    tilt_angle : float
        Tilt angle in degrees for the Wind turbine model.
    yaw_angle : float
        Yaw angle in degrees for the Wind turbine model.
    roll_angle : float
        Roll angle in degrees for the Wind turbine model.
    lidar : str
        String to select which type of lidar is being used. It can take
        "nacelle" for nacelle-mounted lidar, or "hub" for the hub-mounted lidar.

    Raises
    ------
    ValueError
        It raised an error if not proper lidar selection has been made. 

    Returns
    -------
    TYPE array
        Return vector with the new positions based on the rotational matrices
        contribution from tilt, yaw and roll for each lidar.
    """
    # Convert angles from degrees to radians:
    tiltr = np.radians(tilt_angle)
    yawr = np.radians(yaw_angle)
    rollr = np.radians(roll_angle)

    # Rotation matrices:
    Rx = ([1, 0, 0], 
          [0, np.cos(tiltr), -np.sin(tiltr)], 
          [0, np.sin(tiltr), np.cos(tiltr)])

    Ry_nac = ([1, 0, 0],
              [0, 1, 0],
              [0, 0, 1])

    # Changing sign:
    Ry_hub = ([np.cos(rollr), 0, -np.sin(rollr)], 
              [0, 1, 0],
              [np.sin(rollr), 0, np.cos(rollr)])

    Rz = ([np.cos(yawr), -np.sin(yawr), 0],
          [np.sin(yawr), np.cos(yawr), 0],
          [0, 0, 1])
    
    if lidar == 'nacelle':
        hub_pos_nac = np.matmul(np.matmul(Rx, Ry_nac), Rz)
        return hub_pos_nac
    
    elif lidar == 'hub':
        hub_pos_hub = np.matmul(np.matmul(Rx, Ry_hub), Rz)
        return hub_pos_hub
    
    else:
        raise ValueError('Lidar option must be "nacelle" or "hub".\n'
                         'Please select proper option.')
        return
    
    
# %% Function to calculate n beam vector for Nacelle lidar:

    
def calc_n_beam_nac(theta, psi, wsp_opt=True):
    """
    Returns the vector position for the nacelle lidar, based on the
    half-cone angle and the azimuthal angle for the nacelle lidar.

    Parameters
    ----------
    theta : float
        Half-cone angle of the beam in degrees.
    psi : float
        Azimuthal angle of the beam in degrees.

    Returns
    -------
    n_beam_nac : array
        Vector n for the beam for the nacelle lidar.            

    """
    
    # Degrees to radians:
    psi = np.radians(psi)
    theta = np.radians(theta)
    
    if wsp_opt==True:
        n_beam_nac = ([-np.sin(theta) * np.sin(psi),
                       np.cos(theta),
                       np.sin(theta) * np.cos(psi)])
        
    elif wsp_opt==False:
        
        n_beam_nac = ([np.sin(theta) * np.sin(psi),
                   -np.cos(theta),
                   -np.sin(theta) * np.cos(psi)])
        
    else:
        print('wsp must be True if vector is to calculated wsp, and '
              'false if it is to calculate positions.')
    
    return n_beam_nac


# %% Function to calculate n vector for beam hub lidar


def calc_n_beam_hub(theta, psi, azim, wsp_opt=True):
    """
    Returns the vector position for the hub lidar, based on the
    half-cone angle and the initial azimuthal angle for the hub lidar
    and the azimuthal angle of blade 1.

    Parameters
    ----------
    theta : float
        Half-cone angle of the beam in degrees.
    psi : float
        Azimuthal angle of the beam in degrees.

    Returns
    -------
    n_beam_nac : array
        Vector n for the beam for the nacelle lidar.            

    """
    
    # Degrees to radians:
    psi = np.radians(psi)
    theta = np.radians(theta)
    
    if wsp_opt==True:     
    
        n_beam_hub = ([np.sin(theta) * np.sin(azim+psi),
                       np.cos(theta),
                       np.sin(theta) * np.cos(azim+psi)])
        
    elif wsp_opt==False:
        
        n_beam_hub = ([-np.sin(theta) * np.sin(azim+psi),
                   -np.cos(theta),
                   -np.sin(theta) * np.cos(azim+psi)])
        
    else:
        print('wsp must be True if vector is to calculated wsp, and '
              'false if it is to calculate positions.')
    
    return n_beam_hub


# %% Function to calculate the wind speed vector for the nacelle and hub lidar:

def calc_wsp(psi, theta, tilt_angle, yaw_angle, roll_angle, 
             fl, U, xo, yo, zo, df, case, shear=False, **kw):
        
    # Positions for Nacelle and Hub lidar with roll, tilt and yaw contributions:
    hub_pos_nac = rot_matrix_lidar(tilt_angle, yaw_angle, roll_angle, 'nacelle')
    hub_pos_hub = rot_matrix_lidar(tilt_angle, yaw_angle, roll_angle, 'hub')
    
    n_beam_nac = calc_n_beam_nac(theta, psi, wsp_opt=True)
    pos_g_nac = np.array(np.matmul(hub_pos_nac, n_beam_nac))
    
    alpha = kw['alpha']
    z0 = kw['z_hub']
    
    Vx_hub, Vy_hub, Vz_hub = [], [], []
    Vx_nac, Vy_nac, Vz_nac = [], [], []
    V_LOS_nac, V_LOS_hub = [], []
    
    for (azim,
         vx, vy, vz,  # Free wind speed from HAWC2
         hz_hub, hz_nac) in zip(np.radians(df[case]['shaft_rot']),
                                    df[case]['Vx_fw'],
                                    df[case]['Vy_fw'],
                                    df[case]['Vz_fw'], 
                                    df[case]['hl_z'],
                                    df[case]['nac_z']
                                    ):
       
        n_beam_hub = calc_n_beam_hub(theta, psi, azim, wsp_opt=True)
        pos_g_hub = np.array(np.matmul(hub_pos_hub, n_beam_hub))
                                       
        if shear == True:
            vy_add_hub = U * ((hz_hub/z0)**alpha)
            vy_add_nac = U * ((hz_nac/z0)**alpha)

        else:
            vy_add_hub = vy
            vy_add_nac = vy
            
        # Hub lidar:            
        vx_hub = vx * pos_g_hub[0]
        vy_hub = vy_add_hub * pos_g_hub[1]
        vz_hub = vz * pos_g_hub[2]
        
        Vx_hub.append(vx_hub)
        Vy_hub.append(vy_hub)
        Vz_hub.append(vz_hub)
        
        # Nacelle lidar:            
        vx_nac = vx * pos_g_nac[0]
        vy_nac = vy_add_nac * pos_g_nac[1]
        vz_nac = vz * pos_g_nac[2]
                
        Vx_nac.append(vx_nac)
        Vy_nac.append(vy_nac)
        Vz_nac.append(vz_nac)
        
        V_LOS_nac.append(vx_nac + vy_nac + vz_nac)
        V_LOS_hub.append(vx_hub + vy_hub + vz_hub)
            
    wsp = pd.DataFrame({
        'Vx_hub': Vx_hub,
        'Vy_hub': Vy_hub,
        'Vz_hub': Vz_hub,
        'Vx_nac': Vx_nac,
        'Vy_nac': Vy_nac,
        'Vz_nac': Vz_nac,
        'V_LOS_nac': V_LOS_nac,
        'V_LOS_hub': V_LOS_hub
    })
    
    return wsp


# %% Function to calculate position vector for lidar beam:
# TODO Once this function is check, clean it and optimize, since hub_pos can 
# be calculated just once!


def calc_n_vect(psi, theta, fl, df, xo, yo, zo, case,
                tilt_angle, yaw_angle, roll_angle,
                vector, **kw): 
    """
    Function that calculates the theoretical positions in
    the global coordinate system for x, y, z for the
    nacelle and hub lidar. It returns a DataFrame with seven
    columns, including the Hub height of the wind turbine.
    The vector positions can return the final position or
    the unit vector of the position, based on the input
    added to the vector parameter.

    Parameters
    ----------
    psi : float
        Azimuthal angle in degrees.
    theta : float
        Half-cone angle in degrees.
    fl : float
        Focus distance, range distance for the single beam
        lidar in meters.
    df : DataFrame
        DataFrame with the selected channels results
        from HAWC2 aeroelastic simulation, containing the
        information for both lidars.
    xo, yo, zo : float
        Initial offset for the Nacelle lidar in HAWC2.
        Noticed that hub lidar does not have an initial
        offset.
    case : str
        Name of the case, equal to the name of the htc
        file.
    tilt_angle : float
        Tilt angle added to the wind turbine model in
        degrees.
    yaw_angle : float
        Yaw angle added to the wind turbine model in
        degrees.
    roll_angle : float
        Roll angle added to the wind turbine model in
        degrees.
    vector : str
        Values must be pos, to provide the results as the
        final global position vector in meters, or unit to
        return the unit vector for that position.
    **kw : dict
        Dictionary that contains the information for the
        wind turbine model. 

    Raises
    ------
    TypeError
        If the user does not select the vector variable as
        'pos' or 'unit', then it will raise error type,
        to indicate the user of the error.

    Returns
    -------
    theo_pos : DataFrame
        DataFrame with the global positions (total or unit)
        for the nacelle and hub lidar beams, including the
        Hub height of the wind turbine model.

    """
        
    # Degrees to radians:
    psi = np.radians(psi)
    theta = np.radians(theta)
        
    # Initialize vectors:
    x_hub, y_hub, z_hub = [], [], []   
    
    # Hub Height Line:
    z_lin = np.ones(df[case]['Time'].size) * kw['z_hub']
       
    # Nacelle Lidar: 
    init_offset = np.array([xo, yo, zo])
    
    # Position vector Nacelle: 
    wsp_opt=False
    
    n_beam_nac = ([np.sin(theta) * np.sin(psi),
               -np.cos(theta),
               -np.sin(theta) * np.cos(psi)])
    
    #pos_h = init_offset + np.array([x * fl for x in n_beam]) 
    pos_h = np.array([x * fl for x in n_beam_nac])
  
    rot_pos = ([0, kw['shaft_length'], 0]) + init_offset

    # Nacelle Lidar:
    # Rotation matrix around X axis:
    nac_pos = rot_matrix_lidar(tilt_angle, yaw_angle, roll_angle, 'nacelle')  
    rot_pos = np.matmul(nac_pos, rot_pos)
    rot_pos = rot_pos + [0, 0, kw['z_hub']]
    
    pos_g = np.array(rot_pos + np.matmul(nac_pos, pos_h))
    pos_g_unit = pos_g / np.linalg.norm(pos_g)
    
    if vector == 'pos':
        pos_g = pos_g
    elif vector == 'unit':
        pos_g = pos_g_unit
    else:
        raise TypeError('Variable vector must be equal'
                        ' to pos or unit. \n')
                                                                
    x_nac = pos_g[0] * np.ones(df[case]['Time'].size)
    y_nac = pos_g[1] * np.ones(df[case]['Time'].size)
    z_nac = pos_g[2] * np.ones(df[case]['Time'].size)
        
    # Hub Lidar:
    for azim in np.radians(df[case]['shaft_rot']):   
               
        n_beam_hub = ([-np.sin(theta) * np.sin(azim+psi),
                   -np.cos(theta),
                   -np.sin(theta) * np.cos(azim+psi)])
        
        pos_h = np.array([x * fl for x in n_beam_hub])
        rot_pos = ([0, kw['shaft_length'], 0])
        hub_pos = rot_matrix_lidar(tilt_angle, yaw_angle, roll_angle, 'hub') 
        rot_pos = np.matmul(hub_pos, rot_pos)
        rot_pos = rot_pos + [0, 0, kw['z_hub']]
        pos_g = np.array(rot_pos + 
                         np.matmul(hub_pos, pos_h))
        pos_g_unit = pos_g / np.linalg.norm(pos_g) 
        
        if vector == 'pos':
            pos_g = pos_g
        elif vector == 'unit':
            pos_g = pos_g_unit
        else:
            raise TypeError('Variable vector must be equal'
                            ' to pos or unit. \n')
        
        x_hub.append(pos_g[0])
        y_hub.append(pos_g[1])
        z_hub.append(pos_g[2])
        
    theo_pos = dict(x_theo_nac = x_nac,
                    y_theo_nac = y_nac,
                    z_theo_nac = z_nac,
                    x_theo_hub = x_hub,
                    y_theo_hub = y_hub,
                    z_theo_hub = z_hub,
                    z_hub = z_lin)
    
    theo_pos = pd.DataFrame(theo_pos) 
        
    return theo_pos


# %% Function to plot wind speeds, calling specific component:


def plot_wsp_ind(U, comp, psi, theta, tilt_angle, yaw_angle,
                 roll_angle, fl, xo, yo, zo, df, case, fname,
                 axis, limit, shear, colors, title, **kw):

    wsp = calc_wsp(psi, theta, tilt_angle, yaw_angle,
                   roll_angle, fl, U, xo, yo, zo, df, case,
                   shear, **kw)
    
    axis_x, axis_label = get_axis(df, case, axis)

    if comp in ['Vy', 'Vx', 'Vz']:
        y_label = 'Wind Speed \n '+comp+' [m/s]'
        prefix = 'wsp_'+comp+'_'
        plot_common(axis, limit, psi, theta, fl, df, case,
                    fname, wsp, comp, title, y_label,
                    prefix, colors)
    elif comp in 'V_LOS':
        plot_wsp(axis, limit, psi, theta, fl, df, case,
                        fname, wsp, title, colors)        
    else: 
        raise TypeError('Components must be Vx,'
                        ' Vy, Vz or V_LOS. \n')

    return


# %% Function to get axis for plots:


def get_axis(df, case, axis):
    """
    Function that provides the x axis and label for the
    plots, depending on user selection.

    Parameters
    ----------
    df : array
        Wind speed values that will be plotted.
    case : int
        Beam number that is being plotted, for the specific
        case in study.
    axis : str
        It can be 'rot' if plots want to be display as a
        function of the rotational speed, or 'time' if plots
        want to be display as a time series.

    Raises
    ------
    TypeError
        If nor 'rot' or 'time' are given by the user,
        it will raised an type error.

    Returns
    -------
    axis_x : array
        Values for shaft rotation in degrees or time in
        seconds, depending on the user selection.
    axis_label : str
        Label for the X axis for the plot, which will be
        'Shaft rotation [degrees]' or 'Time [s]'.

    """
    
    if axis == 'rot':
        axis_x = df[case]['shaft_rot']
        axis_label = 'Shaft rotation [degrees]'
    elif axis == 'time':
        axis_x = df[case]['Time']
        axis_label = 'Time [s]'
    else:
        raise TypeError('Parameters needs to be time or rot.')
    
    return axis_x, axis_label


# %% Function to plot wind speed components individually:


def plot_common(axis, limit, psi, theta, fl, df, case,
                fname, wsp, comp, title, y_label, prefix,
                colors):
    """
    Plots the wind speed components individually. """

    axis_x, axis_label = get_axis(df, case, axis)
    No = 50

    fig, axs = plt.subplots(figsize=(9, 5))
    axs.plot(axis_x, df[case]['nac_LOS'], 'o', markevery=No,
             color=colors[0], label='HAWC2 NL\nLOS wsp')
    axs.plot(axis_x, wsp[comp+'_nac'], color=colors[0],
             label='NL '+comp+'$_{Theo}$')
    axs.plot(axis_x, df[case]['hl_LOS_wgh'], 'o', markevery=No+3,
             color=colors[1], label='HAWC2 HL\nLOS')
    axs.plot(axis_x, wsp[comp+'_hub'], color=colors[1],
             label='HL '+comp+'$_{Theo}$')
    axs.plot(axis_x, df[case][comp+'_fw'], linestyle='--', markevery=No+6,
             color='grey', label='Free\nWind '+comp)

    axs.set_ylabel(y_label, weight='bold', fontsize=14)
    axs.set_xlabel(axis_label, weight='bold', fontsize=14)
    axs.set_xlim(limit[0], limit[1])
    y_min, y_max = find_lim_wsp(wsp[comp+'_hub'],
                                df[case]['hl_LOS_wgh'],
                                df[case][comp+'_fw'], 5)
    axs.set_ylim(y_min, y_max)
    axs.grid(True)
    axs.legend(ncol=5, loc='best')
    axs.set_title(f'Wind Speed {comp} for case {title} \n'
                  f'at \u03b8 = {theta}\u00b0,'
                  f'\u03c8 = {psi}\u00b0 and $f_d$={fl} [m] \n',
                  fontsize=14, weight='bold')
    plt.tight_layout()
    save_figure(fname, case, prefix)
    plt.close()
    
    return


# %% Function to plot wind speed: 


def plot_wsp(axis, limit, psi, theta, fl, df, case,
                fname, wsp, title, colors):

    axis_x, axis_label = get_axis(df, case, axis)
    No = 60

    fig, axs = plt.subplots(figsize=(9, 5))
    axs.plot(axis_x, df[case]['nac_LOS'], 'o', markevery=No,
             color=colors[0], label='HAWC2 NL\nLOS wsp')
    axs.plot(axis_x, wsp['V_LOS_nac'], color=colors[0],
             label='NL V_LOS$_{Theo}$')
    axs.plot(axis_x, df[case]['hl_LOS_wgh'], 'o', markevery=No+3,
             color=colors[1], label='HAWC2 HL\nLOS')
    axs.plot(axis_x, wsp['V_LOS_hub'], color=colors[1],
             label='HL V_LOS$_{Theo}$')
    axs.plot(axis_x, df[case]['Vy_fw'], linestyle='--',
             color='grey', label='Free\nWind Vy')

    axs.set_ylabel('Wind Speed V_LOS [m/s]', weight='bold', fontsize=14)
    axs.set_xlabel(axis_label, weight='bold', fontsize=14)
    axs.set_xlim(limit[0], limit[1])
    y_min, y_max = find_lim_wsp(wsp['V_LOS_hub'],
                                df[case]['hl_LOS_wgh'],
                                df[case]['Vy_fw'], 5)
    axs.set_ylim(y_min, y_max)
    axs.grid(True)
    axs.legend(ncol=6, loc='best')
    axs.set_title(f'Wind Speed V_LOS for case {title} \n'
                  f'at \u03b8 = {theta}\u00b0,'
                  f'\u03c8 = {psi}\u00b0 and $f_d$={fl} [m] \n',
                  fontsize=12, weight='bold')
    prefix = 'V_LOS_'
    plt.tight_layout()
    save_figure(fname, case, prefix)
    plt.close()
    
    return



# %% Function to find the limites for y axis for plots:


def find_lim_wsp(df1, df2, df3, add):
    """
    Finds the limit for the y axis in the figures, based on
    three arrays provide. 

    Parameters
    ----------
    df1, df2, df3: array
        Values of the wind speeds that are being compared in
        the figure.
    add : int
        Value added to the min and max values to return the
        limit for Y axis for plots.

    Returns
    -------
    y_min, y_max: float
        Minimun and maximum value minus / plus the added
        integer.

    """
    dataframes = [df1, df2, df3]
    
    min_values = [np.nanmin(df) if df.size > 0 else 0 for df in dataframes]
    max_values = [np.nanmax(df) if df.size > 0 else 0 for df in dataframes]

    y_min = min(min_values) - add
    y_max = max(max_values) + add

    return y_min, y_max



# %% Function to plot wind speeds, calling specific component:


def plot_wsp_ind(U, comp, psi, theta, tilt_angle, yaw_angle,
                 roll_angle, fl, xo, yo, zo, df, case, fname,
                 axis, limit, shear, colors, title, **kw):

    wsp = calc_wsp(psi, theta, tilt_angle, yaw_angle,
                   roll_angle, fl, U, xo, yo, zo, df, case,
                   shear, **kw)
    
    axis_x, axis_label = get_axis(df, case, axis)

    if comp in ['Vy', 'Vx', 'Vz']:
        y_label = 'Wind Speed \n '+comp+' [m/s]'
        prefix = 'wsp_'+comp+'_'
        plot_common(axis, limit, psi, theta, fl, df, case,
                    fname, wsp, comp, title, y_label,
                    prefix, colors)
    elif comp in 'V_LOS':
        plot_wsp(axis, limit, psi, theta, fl, df, case,
                        fname, wsp, title, colors)        
    else: 
        raise TypeError('Components must be Vx,'
                        ' Vy, Vz or V_LOS. \n')

    return


# %% Plot positions for nacelle and hub lidar, combined.


def plot_positions(psi, theta, fl, df, xo, yo, zo, case, fname, axis, limit,
                   colors, title, tilt_angle, yaw_angle, roll_angle,
                   lidar, **kw_nrel5mw):
    
    No = 60
    axis_x, axis_label = get_axis(df, case, axis)
    theo_pos = calc_n_vect(psi, theta, fl, df, xo, yo, zo, case,
                           tilt_angle, yaw_angle, roll_angle, vector='pos', 
                           **kw_nrel5mw)
        
    if lidar == 'hub':
        data_labels = [
             ('hl_x', 'x_theo_hub', 'X$_G$', 'X$_{Theo}$'),
             ('hl_y', 'y_theo_hub', 'Y$_G$', 'Y$_{Theo}$'),
             ('hl_z', 'z_theo_hub', 'Z$_G$', 'Z$_{Theo}$')]
        label_lidar = 'Hub'
             
    elif lidar == 'nac':
        data_labels = [
            ('nac_x', 'x_theo_nac', 'X$_G$', 'X$_{Theo}$'),
            ('nac_y', 'y_theo_nac', 'Y$_G$', 'Y$_{Theo}$'),
            ('nac_z', 'z_theo_nac', 'Z$_G$', 'Z$_{Theo}$')]
        label_lidar = 'Nacelle'
        
    else:
        raise TypeError('Lidar must be equal to nac or hub. \n')
    
    fig, axs = plt.subplots(nrows=1, ncols=1, figsize=(8, 6))
    
    for data_label, color in zip(data_labels, colors):
        axs.plot(axis_x, df[case][data_label[0]], 'o', markevery=No,
                 color=color, label=data_label[2])
        axs.plot(axis_x, theo_pos[data_label[1]], 
                 color=color, label=data_label[3])
        
    axs.plot(axis_x, theo_pos['z_hub'], linestyle='--', color='grey',
                label='Hub\nHeight')
    
    axs.set_xlabel(axis_label, weight='bold', fontsize=14)
    axs.set_ylabel('X, Y, Z [m]', weight='bold', fontsize=14)
    axs.set_xlim(limit[0], limit[1])
    
    if lidar == 'hub':
        min_y = min([min(theo_pos['x_theo_hub']), 
                     min(theo_pos['y_theo_hub']),
                     min(theo_pos['z_theo_hub'])
                     ])
        
        max_y = max([max(theo_pos['x_theo_hub']), 
                     max(theo_pos['y_theo_hub']),
                     max(theo_pos['z_theo_hub'])
                     ])
        
        ylim = [min_y - 10,
                max_y + 10]
        
        axs.set_ylim(ylim[0], ylim[1])
        
    elif lidar == 'nac':
        
        min_y = min([min(theo_pos['x_theo_nac']), 
                     min(theo_pos['y_theo_nac']),
                     min(theo_pos['z_theo_nac'])
                     ])
        
        max_y = max([max(theo_pos['x_theo_nac']), 
                     max(theo_pos['y_theo_nac']),
                     max(theo_pos['z_theo_nac'])
                     ])
        
        ylim = [min_y - 10,
                max_y + 10]
        
        axs.set_ylim(ylim[0], ylim[1])
    else:
        axs.set_ylim(-130, 50)
    
    axs.grid(True)
    
    plt.suptitle(f'{label_lidar} lidar global positions for \n'
                 f'{title} \n'
                 f'at \u03b8 = {theta}\u00b0, \u03c8 = {psi}\u00b0,'
                 f' $f_d$={fl} [m] \n', weight='bold', fontsize=14)
    
    fig.legend(loc='center', fontsize=12, bbox_to_anchor=(0.5, 0.05),
               borderaxespad=0.1, ncol=7)
        
    plt.subplots_adjust(top=0.85, hspace=0.4)
    plt.subplots_adjust(bottom=0.2)
    
    prefix = f'{label_lidar}_pos_combined_'
    save_figure(fname, case, prefix)
    plt.close()
    
    return


# %%


def calc_diff_pos_versions(table, case1, case2):
    """
    Calculates the difference between the global positions for the Nacelle
    lidar between HAWC2 version v12.9 and v.13.1.

    Parameters
    ----------
    table : TYPE
        DESCRIPTION.
    case1 : TYPE
        DESCRIPTION.
    case2 : TYPE
        DESCRIPTION.

    Returns
    -------
    data : TYPE
        DESCRIPTION.

    """
        
    round_n = 5
    
    diff_x = (table[case1]['nac_x']['Mean'] - 
              table[case2]['nac_x']['Mean'])

    diff_y = (table[case1]['nac_y']['Mean'] - 
              table[case2]['nac_y']['Mean'])

    diff_z = (table[case1]['nac_z']['Mean'] - 
              table[case2]['nac_z']['Mean'])
    
    diff_VLOS = (table[case1]['nac_VLOS']['Mean'] - 
              table[case2]['nac_VLOS']['Mean'])

    data = {'dx': round(diff_x, round_n), 
            'dy': round(diff_y, round_n),
            'dz': round(diff_z, round_n),
            'VLOS': round(diff_VLOS, round_n)
                    }    
    return data


# %% Function to assert positions and LOS wind speeds:


def assert_pos_and_wsp(cases, wsp_df, n_vector_df, tolerance_pos,
                              tolerance_wsp, output_file):
    """
    Function to assert the Global positions and line-of-sight velocity
    of all the cases for the Nacelle and Hub lidar, compared with the
    theoretical values and the HAWC2 simulation values. 
    The comparison is performed with a tolerance provided by the user for the 
    positions and the wind speed.
    It will create a text file with the results. At the end of the file, a
    summary of the failed cases can be found. 

    Parameters
    ----------
    cases : dict
        Dictionary with all the cases with the name of the htc file, 
        with DataFrames for each beam evaluation, that contain the values
        from HAWC2 simulation results. 
    wsp_df : Dict
        Dictionary, where each case has DataFrames for each beam case, with the 
        theoretical values for the line-of-sight wind speed for the nacelle and
        hub lidar. 
    n_vector_df : Dict
        Dictionary, where each case has DataFrames for each beam case, with the 
        theoretical values for the Global positions for the nacelle and
        hub lidar.
    tolerance_pos : float
        Tolerance for the Global positions for the assertion statement.
    tolerance_wsp : float
        Tolerance for the line-of-sight weighted wind speed for the assertion
        statement.
    output_file : str
        Name of the text file to save the information of the evaluation.
    
    """
    def test_val_theo(f, val, theo, test_str, abs_tol):
        """Compare two arrays and write if they don't match.
        test_str is check name, e.g. "Nacelle Lidar Global positions x"
        f is open file handle to write to.
        """
        if np.allclose(val, theo, atol=abs_tol):
            f.write(f"    - {test_str}: Pass\n")
        else:
            maxdiff = np.abs(val - theo).max()
            f.write(f"    - {test_str}: Failed\n")
            f.write(f"        Max diff: {maxdiff:.4f}, tol: {abs_tol:.4f}\n")
            passed = False
    
    all_asserts_pass = True
    failed_cases = []
    
    now = datetime.datetime.now()
    
    with open(output_file, 'w') as f:
        f.write(f"Welcome to the results file.\n")
        f.write(f"Evaluation date and time: {now.strftime('%Y-%m-%d %H:%M')}\n \n")
        
        for case_name, case_data in cases.items():
            
            if 'v12' in case_name or 's927' in case_name:
                f.write(f"Case name: {case_name}:\n")
                f.write('  This case is not evaluated in the assert function. \n \n')
                
            else: 
                f.write(f"Case name: {case_name}:\n")
            
                for case, case_values in case_data.items():  # Iterate over cases in case_data
                    f.write(f"  For case = {case}:\n")
                    passed = True  # Track whether all assertions for this case pass
                
                    # Nacelle lidar positions
                    test_str = 'Nacelle Lidar Global positions x'
                    val = case_values['nac_x']
                    theo = n_vector_df[case_name][case]['x_theo_nac']
                    test_val_theo(f, val, theo, test_str, tolerance_pos)
                    
                    test_str = 'Nacelle Lidar Global positions y'
                    val = case_values['nac_y']
                    theo = n_vector_df[case_name][case]['y_theo_nac']
                    test_val_theo(f, val, theo, test_str, tolerance_pos)
                    
                    test_str = 'Nacelle Lidar Global positions z'
                    val = case_values['nac_z']
                    theo = n_vector_df[case_name][case]['z_theo_nac']
                    test_val_theo(f, val, theo, test_str, tolerance_pos)
                
                    # Nacelle lidar wind speeds
                    test_str = 'Nacelle Lidar LOS wind speeds'
                    val = case_values['nac_LOS']
                    theo = wsp_df[case_name][case]['V_LOS_nac']
                    test_val_theo(f, val, theo, test_str, tolerance_wsp)

                    # Hub lidar positions
                    test_str = 'Hub Lidar Global positions x'
                    val = case_values['hl_x']
                    theo = n_vector_df[case_name][case]['x_theo_hub']
                    test_val_theo(f, val, theo, test_str, tolerance_pos)

                    test_str = 'Hub Lidar Global positions y'
                    val = case_values['hl_y']
                    theo = n_vector_df[case_name][case]['y_theo_hub']
                    test_val_theo(f, val, theo, test_str, tolerance_pos)

                    test_str = 'Hub Lidar Global positions z'
                    val = case_values['hl_z']
                    theo = n_vector_df[case_name][case]['z_theo_hub']
                    test_val_theo(f, val, theo, test_str, tolerance_pos)

                    # Hub lidar wind speeds
                    test_str = 'Hub Lidar Wind speeds'
                    val = case_values['hl_LOS_wgh']
                    theo = wsp_df[case_name][case]['V_LOS_hub']
                    test_val_theo(f, val, theo, test_str, tolerance_wsp)

                    if passed:
                        f.write("    All assertions passed for this case.\n \n")
                    else:
                        f.write("    Some assertions failed for this case.\n ")
                        f.write("    =============  FAILED ===============\n \n")
                        all_asserts_pass = False
                        failed_cases.append((case_name, case))  # Append the failed case name
    
        if not all_asserts_pass:
            f.write("Summary of FAILED cases:\n")
            for case_name, case_num in failed_cases:
                f.write(f"- {case_name}, for case = {case_num}\n")
                print("Summary of FAILED cases:")
            for case_name, case_num in failed_cases:
                print(f"- {case_name}, for case = {case_num}")

    return 


# %% Function to plot wsp and positions of all cases:


def plot_wsp_and_pos(cases, axis, limit, hub_lidar, kw_nrel5mw, wsp=True, pos=True):
    
    # Defining colors: 
    colors = ['dodgerblue', 'darkviolet', 'crimson', 'yellowgreen',
          'darkorange', 'royalblue', 'magenta', 'coral', 'gold']
       
    wsp_res = {}
    n_vector_res = {}
    
    for case_name, case_data in cases.items(): 
        print(case_name)
        U = 11.0
        case = 0
        comp = 'V_LOS'
        shear = False
        tilt_ang, yaw_ang, roll_ang = 0, 0, 0
        xo, yo, zo = 0, 0, 0
        title = split_title(case_name)
        theta = hub_lidar['theta']
        ranges = hub_lidar['ranges']
                   
        if 'tilt_10' in case_name:
            tilt_ang = 10.0
            comp = 'V_LOS'
        
        elif 'roll_10' in case_name:
            roll_ang = 10.0
            comp = 'V_LOS'

        elif 'yaw_10' in case_name:
            yaw_ang = 10.0
            comp = 'V_LOS'
        
        elif 'turb_cte' in case_name:
            comp = 'V_LOS'
            
        elif ('nrel5mw_wsp_11.0_yaw_90' in case_name or
              'nrel5mw_wsp_11.0_yaw_270' in case_name):
            comp = 'Vx'
            
        elif 'shear' in case_name:
            shear = True
            
        wsp_l = {}
        n_vector_l = {}
            
        for psi, theta, fl in zip(hub_lidar['psi'], 
                                  theta, ranges):
            
            if wsp == True:
                wsp_array = calc_wsp(psi, theta, tilt_ang, yaw_ang, roll_ang, 
                              fl, U, xo, yo, zo, case_data, case,
                              shear, **kw_nrel5mw)
                
                vector = 'pos'
                n_vector = calc_n_vect(psi, theta, fl, case_data,
                                       xo, yo, zo, case,
                                       tilt_ang, yaw_ang, roll_ang,
                                       vector, **kw_nrel5mw)
                
                plot_wsp_ind(U, comp, psi, theta, 
                             tilt_ang, yaw_ang, roll_ang, fl,
                             xo, yo, zo, case_data, case, case_name, axis,
                             limit, shear, colors, title, **kw_nrel5mw)

            if pos == True:
                plot_positions(psi, theta, fl, case_data, xo, yo, zo, case,
                               case_name, axis, limit, colors, title,
                               tilt_ang, yaw_ang, roll_ang, lidar='hub',
                               **kw_nrel5mw)
     
                plot_positions(psi, theta, fl, case_data, xo, yo, zo, case,
                               case_name, axis, limit, colors, title,
                               tilt_ang, yaw_ang, roll_ang, lidar='nac',
                               **kw_nrel5mw)

            wsp_l[case] = wsp_array
            n_vector_l[case] = n_vector
            case = case + 1
            
        wsp_res[case_name] = wsp_l
        n_vector_res[case_name] = n_vector_l
        
    return wsp_res, n_vector_res


def plot_nl_hl_fw(kw_nrel5mw, fname='nrel5mw_wsp_11.0_turb_s927_v13.1'):
    """Compare the time series of the nacelle lidar, hub lidar, and
    free wind speed.
    """
    hawc2_path = kw_nrel5mw['path_res'] + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    h2res = read_h2.ReadGtsdf()

    idx_nl = 93 - 1  # pdap indices are from 1. nacelle lidar
    idx_hl = 143 - 1  # hub lidar
    idx_fw = 177 - 1  # free wind

    t = h2res[:, 0]
    nl = h2res[:, idx_nl]
    hl = h2res[:, idx_hl]
    fw = h2res[:, idx_fw]

    plt.rcParams["figure.dpi"] = 200
    fig, ax = plt.subplots(figsize=(8, 3.5))

    ax.plot(t, fw, color='b', label='Free wind')
    ax.plot(t, nl, '-.', color='k', marker='*', markevery=200, 
            label='Nacelle lidar')
    ax.plot(t, hl, '--', color='k', marker='o', mfc='none', markevery=(100, 200),
            label='Hub lidar')

    ax.grid()
    ax.legend()
    ax.set(xlim=[0, 50])
    ax.set_ylabel('Wind speed [m/s]', weight='bold', fontsize=11)
    ax.set_xlabel('Time [s]', weight='bold', fontsize=11)
    ax.set_title('Comparison of hub and nacelle lidars to free wind,\n'
                 'turbulent inflow with HAWC2 v13.1\n',
                 fontsize=12, weight='bold')

    fig.tight_layout()

    prefix = 'nl_hl_fw_'
    case = 5
    save_figure(fname, case, prefix)
