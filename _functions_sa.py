# -*- coding: utf-8 -*-
"""Helper functions for the sensitivity analysis.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
from wetb.hawc2.Hawc2io import ReadHawc2

import _functions as func


def find_dv(DeltaL=24.75):
    """
    Finds the dv values to be evaluated in the sensitivity analysis, based on
    the value provided by DeltaL

    Parameters
    ----------
    DeltaL : float, optional
        Lidar parameter provided by the manufacturer.
        The default is 24.75.

    Returns
    -------
    dv : array
        Half-with of the integration volume: dv * DeltaL [m]
    dv_norm : array
        Half-with of the integration volume divided by DeltaL [-], which
        gives the value of dv as an input for HAWC2.

    """
    dv_norm = (np.append(np.linspace(0, 20, 11), np.linspace(25, 100, 16)) /
               DeltaL)
    dv =  np.append(np.linspace(0,20, 11), np.linspace(25, 100, 16))
    return dv, dv_norm


def plot_convergence_dv(kw_nrel5mw, fname='nrel5mw_wsp_11.0_turb_s927_v13.1',
                        DeltaL=24.75, savefig=True, figdir='./'):
    """Create plot of convergence of Vlos RMSE w.r.t. dv."""
    IDX_LOS_LO = 188 - 1  # pdap index (from 0) of first LOS in sensitivity block
    BEAM_START = 8  # label in hawc2 of first beam
    BEAM_END = 34  # label in hawc2 of last beam
    CHAN_PER_BEAM = 8  # how many hawc2 output channels per beam

    hawc2_path = kw_nrel5mw['path_res'] + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    h2res = read_h2.ReadGtsdf()

    no_beams = BEAM_END - BEAM_START + 1  # number of beams
    idx_los_hi = IDX_LOS_LO + CHAN_PER_BEAM * (no_beams - 1)  # pdap idx highest

    # get the highest-fidelity time series
    t = h2res[:, 0]
    los_nom = h2res[:, idx_los_hi]

    # iterate through lower fidelities, calculating RMSE with the highest-fidelity
    rmses = np.full(no_beams, np.nan)
    for i in range(no_beams):
        idx_beam = IDX_LOS_LO + 8 * i
        los_beam = h2res[:, idx_beam]
        rmses[i] = np.sqrt(np.mean((los_beam - los_nom)**2))

    # make plot
    dv, dv_norm = find_dv(DeltaL=DeltaL)
    dv_norm = np.round(dv_norm, 2)
    index_hw = dv_norm.astype(str) 

    title = 'Half-width of the integration volume: dv [-]'
    title_cte = ', with N = 200.'

    # initialize figure
    plt.rcParams["figure.dpi"] = 150
    fig, ax = plt.subplots(figsize=(12,5))
    
    ax.plot(index_hw, np.zeros_like(rmses), linestyle='--', color='grey')
    ax.plot(index_hw, rmses)
    ax.scatter(index_hw, rmses, marker='o', color='crimson', zorder=5)

    ax.set_xlabel(title, weight='bold', fontsize=14)
    ax.set_ylabel('RMSE for hub lidar V$_{LOS}$',
                  weight='bold', fontsize=14)
    ax.set_title('Sensitivity analysis for HAWC2 Hub Lidar implementation \n'
                 + title + title_cte, weight='bold', fontsize=14)

    fig.tight_layout()

    if savefig:
        func.check_path(figdir)
            
        plot_name = figdir + 'dv_half_width_sens_analysis.png'
        plt.savefig(plot_name)

    return fig


def plot_convergence_N(kw_nrel5mw, fname='nrel5mw_wsp_11.0_turb_s927_v13.1',
                       DeltaL=24.75, savefig=True, figdir='./'):
    """Make convergence plot versus N."""
    IDX_LOS_LO = 404 - 1  # pdap index (from 0) of first LOS in sensitivity block
    BEAM_START = 35  # label for first beam
    BEAM_END = 51  # label for last beam
    CHAN_PER_BEAM = 8  # how many hawc2 output channels per beam

    hawc2_path = kw_nrel5mw['path_res'] + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    h2res = read_h2.ReadGtsdf()

    # ----- sensitivity on half-width of integration volume dv, N = 100 -----
    no_beams = BEAM_END - BEAM_START + 1
    idx_los_hi = IDX_LOS_LO + CHAN_PER_BEAM * (no_beams - 1)

    # get the highest-fidelity time series
    t = h2res[:, 0]
    los_nom = h2res[:, idx_los_hi]

    # iterate through lower fidelities, calculating RMSE with the highest-fidelity
    rmses = np.full(no_beams, np.nan)
    for i in range(no_beams):
        idx_beam = IDX_LOS_LO + 8 * i
        los_beam = h2res[:, idx_beam]
        rmses[i] = np.sqrt(np.mean((los_beam - los_nom)**2))

    # make plot
    index_N = ['0', '1', '5', '10', '15', '20', '30', '40', '60', '80',
               '100', '150', '200', '250', '300', '350', '400']

    title = 'Number of integration points'
    title_cte = ', with dv = 3.0 [-].'
    skip_first = 2

    # Plot line for Sensitivity Analysis of Half-with:
    plt.rcParams["figure.dpi"] = 150
    fig, axs = plt.subplots(figsize=(12,5))

    axs.set_xlabel(title, weight='bold', fontsize=14)
    axs.set_ylabel('RMSE for hub lidar V$_{LOS}$',  weight='bold', fontsize=14)
    axs.set_title('Sensitivity analysis for HAWC2 Hub Lidar implementation \n'
                    + title + title_cte, weight='bold', fontsize=14)

    plt.plot(index_N[skip_first:], np.zeros_like(rmses[skip_first:]), linestyle='--', color='grey')
    plt.plot(index_N[skip_first:], rmses[skip_first:])
    plt.plot(index_N[skip_first:], rmses[skip_first:], 'o', color='crimson', zorder=5)

    fig.tight_layout()
    
    if savefig:
        func.check_path(figdir)
            
        plot_name = figdir + 'N_integration_number_sens_analysis.png'
        plt.savefig(plot_name)

    return fig


def save_res_sens_analysis(fname, beam_no, **kw):
    """
    Saves the results from the sensitivity analysis in a DataFrame.

    Parameters
    ----------
    fname : str
        Name of the file result from HAWC2 simulation.
    beam_no : int
        Number of total beams or cases to be evaluated in the sensitivy
        analysis.
    **kw_nrel5mw : dict
        Dictionary with data for the wind turbine model.

    Returns
    -------
    res : DataFrame
        DataFrame with: Time [s], shaft rotation angle [deg], Hub lidar
        global coordinate positions x, y, z [m]. Hub lidar weighted wind speed
        and Hub lidar nominal wind speed [m/s]. 
        
    """
    hawc2_path = kw['path_res'] + fname + '.hdf5'
    read_h2 = ReadHawc2(hawc2_path)
    hawc2_res = read_h2.ReadGtsdf()
    # Creating the DataFrame:
    res = dict()
    for i in range(beam_no):
        time = hawc2_res[:, 0]
        shaft_rot = hawc2_res[:, 1]
        # Sensitivity Analysis:
        idx = kw['idx_sens_hl'] + i*8
        hl_x = hawc2_res[:, idx]
        hl_y = hawc2_res[:, idx + 1]
        hl_z = hawc2_res[:, idx + 2]
        hl_LOS_wgh = hawc2_res[:, idx +3]
        hl_LOS_nom = hawc2_res[:, idx + 4]
        fw_Vy = hawc2_res[:, idx + 6]
        # Stacking results:
        res[i] = np.column_stack((time, shaft_rot,
                              hl_x, hl_y, hl_z, hl_LOS_wgh, hl_LOS_nom, fw_Vy))
        # Columns name for DataFrame:
        column_labels = ['Time', 'shaft_rot',
                         'hl_x', 'hl_y', 'hl_z', 'hl_LOS_wgh', 'hl_LOS_nom', 'Vy']
        res[i] = pd.DataFrame(res[i], columns=column_labels)
    path = '.\\files\\'
    json_file = path + fname + '.pickle'
    with open(json_file, 'wb') as file:
        pickle.dump(res, file)
    print(f'DataFrame has been saved as: {json_file}. \n')
    return res


def plot_comp_wgh_nom_fw_subplot_v2(axs, file_name, case_num, case, N, x_lim,
                                    xlabel=True, legend=True,
                                    title=True, **kw):
    """
    Plots the time series in a specific limit (x_lim), for the weighted wind
    speed and the nominal wind speed, for a specific case.

    Parameters
    ----------
    axs : matplotlib.axes.Axes
        The axes to plot on.
    file_name : str
        File name of the HAWC2 simulation with the results for the sensitivity
        analysis. 
    case_num : int
        Number of cases studied in the sensitiity analysis. 
    case : int
        Specific case number that ones to be plotted for analaysis. 
    x_lim : array
        Vector with initial time and final time, for the limits of the plot.
    **kw : dict
        Dictionary with the values for the wind turbine model.

    Returns
    -------
    None.
    """
    
    sens_analysis_df = save_res_sens_analysis(file_name, case_num, **kw)

    axs.plot(sens_analysis_df[case]['Time'], sens_analysis_df[case]['hl_LOS_wgh'], 
                 label='Weighted V$_{LOS}$')
    axs.plot(sens_analysis_df[case]['Time'], sens_analysis_df[case]['hl_LOS_nom'],
             label='Nominal V$_{LOS}$')
    axs.plot(sens_analysis_df[case]['Time'], sens_analysis_df[case]['Vy'], '--',
             label='Free Wind Vy')
    axs.set_ylabel('Wind speed [m/s]', weight='bold', fontsize=14)
    if xlabel:
        axs.set_xlabel('Time [s]', weight='bold', fontsize=14)
    axs.set_xlim(x_lim[0], x_lim[1])
    axs.grid(True)
    if legend:
        axs.legend(ncol=1, loc='best')
    if title == True:
        axs.set_title(f'Number of integrations N={N}', fontsize=12, weight='bold')
                
    return
