# -*- coding: utf-8 -*-
"""
Created on Mon May 15 12:16:53 2023

@author: espa
"""

from _values import kw_turbgen
from _functions import generate_htc_files

# %% Creates htc files for all cases:

# Difference between 13.0 and 13.1 is the nose_cone_lidar to hub_lidar
orig_htc_v12 = '.\\nrel_5mw\\htc\\nrel_5mw_master_v12.htc' 
orig_htc_v13 = '.\\nrel_5mw\\htc\\nrel_5mw_master_v13.htc' 

prefix = 'nrel5mw_wsp_'
wsp = 11.0

cases_list_v13 = ['positive', 'negative',
              'yaw_90', 'yaw_270',
              'shear_0.2',
              'turb_s927_v13.1', 'turb_cte', 
              'roll_10', 'tilt_10', 'yaw_10',
              'positive_flex_tower']

cases_list_v12 = ['roll_10_v12', 'tilt_10_v12', 'yaw_10_v12', 'positive_flex_tower_v12']

cases_v12 = {}
cases_v13 = {}

for i, case in enumerate(cases_list_v12, start=1):
    cases_v12[f'Case{i}'] = prefix + str(wsp) + '_' + case
    
for i, case in enumerate(cases_list_v13, start=1):
    cases_v13[f'Case{i}'] = prefix + str(wsp) + '_' + case

generate_htc_files(orig_htc_v12, wsp, cases_v12, **kw_turbgen)
generate_htc_files(orig_htc_v13, wsp, cases_v13, **kw_turbgen)

