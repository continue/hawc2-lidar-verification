"""Create plots for convergence versus dv and N.
"""
import matplotlib.pyplot as plt

import _functions as func
import _functions_sa as fsa
from _values import kw_nrel5mw


FIG_DIR = './figures/06_sensitivity_analysis/'

# convergence versus dv
fsa.plot_convergence_dv(kw_nrel5mw, figdir=FIG_DIR)

# convergence versus N
fsa.plot_convergence_N(kw_nrel5mw, figdir=FIG_DIR)


# Plot time series for three different integration numbers N:
case_num = 51 - 8 + 1  # total number of hub lidar beams
file_name = '.\\nrel5mw_wsp_11.0_turb_s927_v13.1'

x_lim = [100, 150]
cases = [36 - 8, 37 - 8, 45 - 8]  # indices of beams to plot (offset from initial hub-lidar beam)
int_n = [1, 5, 100]  # corresponding integration numbers

# Create subplots
fig, axs = plt.subplots(len(cases), 1, figsize=(10, 4*len(cases)))  # Change the figure size if needed

for ax, case, N in zip(axs[:-1], cases[:-1], int_n[:-1]):  # Exclude the last subplot
    fsa.plot_comp_wgh_nom_fw_subplot_v2(ax, file_name, case_num, case, N,
                                        x_lim, xlabel=False, legend=False,
                                        title=True, **kw_nrel5mw)

# Add x-label and legend to the last subplot
fsa.plot_comp_wgh_nom_fw_subplot_v2(axs[-1], file_name, case_num, cases[-1],
                                    int_n[-1], x_lim, xlabel=True,
                                    legend=True, title=True, **kw_nrel5mw)

plt.tight_layout()

save = True
if save:
    fname = 'time_series_with_diff_N'
    func.check_path(FIG_DIR)
        
    plot_name = FIG_DIR + fname + '.png'
    plt.savefig(plot_name, dpi=150)
