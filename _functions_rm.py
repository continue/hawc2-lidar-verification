# -*- coding: utf-8 -*-
"""
Created on Mon Nov 27 13:47:00 2023

@author: espa
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import _functions as func

# %%

def plot_rel_vel(case_rel_motion, fs, t_start, t_end, save=True):
    """
    Function that plot a the hub global velocity and the LOS hub-lidar.
    
    """
    # Note: Beam 4 is the case of the beam looking directly into the wind.
    beam_no = 4
    
    t = np.arange(t_start, t_end, 1/fs)  # Time vector
    
    # Normalization of the data:
    LOS_norm = ((case_rel_motion[beam_no]['hl_LOS_nom'] -
                 case_rel_motion[beam_no]['hl_LOS_nom'].mean()) /
                 case_rel_motion[beam_no]['hl_LOS_nom'].std())

    hub_Vy_norm = ((case_rel_motion[beam_no]['hub_vel_y'] - 
                    case_rel_motion[beam_no]['hub_vel_y'].mean()) / 
                    case_rel_motion[beam_no]['hub_vel_y'].std())

    fig, ax1 = plt.subplots(figsize=(12, 6))
    ax1.set_xlabel('Time [s]', fontsize=12, fontweight='bold')
    ax1.set_ylabel('Normalized LOS weighted wind speed [m/s]', color='black', fontsize=12, fontweight='bold')
    ax1.plot(t, LOS_norm, linestyle = '--', color='black', label='Normalized LOS wind speed')
    ax1.tick_params(axis='y', labelcolor='black')

    # Create a second y-axis on the right side
    ax2 = ax1.twinx()
    ax2.set_ylabel('Normalized Hub global velocity, Vy [m/s]', color='blue', fontsize=12, fontweight='bold')
    ax2.plot(t, hub_Vy_norm, 'blue', label='Normalized Hub Velocity Vy')
    ax2.tick_params(axis='y', labelcolor='blue')

    plt.title("Normalized LOS wind speed and hub global velocity (Vy)", fontsize=16, fontweight='bold')
    plt.xlim([100, 105])
    plt.grid(True)
    plt.tight_layout()

    # Create a combined legend for both axes
    lines, labels = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax1.legend(lines + lines2, labels + labels2, loc='upper center', 
               bbox_to_anchor=(0.5, -0.13), fontsize=13,
               fancybox=True, shadow=True, ncol=2)

    # Adjust the layout to make room for the legend
    plt.subplots_adjust(bottom=0.20)

    if save:
        fname = 'comparison_los_hub_vel'
        path = '.\\figures\\06_relative_motion\\'
        func.check_path(path)
        
        plot_name = path + fname + '.png'
        plt.savefig(plot_name)

    plt.show()
        
    return

# %%

def calculate_psd(t, x, return_X=False):
    """Get the one-sided PSD of signal"""
    #  It returns the Discrete Fourier Transform sample frequencies
    # for a real-valued input. 
    dt = (t[-1] - t[0]) / (t.size - 1)
    T = t[-1] - t[0] + dt
    df = 1 / T
    f = np.fft.rfftfreq(x.size, d=dt)
    X = np.fft.rfft(x) / x.size
    Sx = 2 * np.abs(X)**2 / df
    if return_X:
        return f, Sx, X
    else:
        return f, Sx
    
    
# %%


def plot_psd_rel_motion(case_rel_motion, fs, t_start, t_end, save=True):
    """
    Function to plot the PSD for the Tower bottom fore-aft (MxTB) and the 
    hub-lidar LOS wind speed. It takes a dictionary, with all the cases,
    the beam number that want to be evaluated, the sampling frequency of the
    simulation, and the start and end time. 
    """
    
    # Note: Beam 4 is the case of the beam looking directly into the wind.
    beam_no = 4
    
    t = np.arange(t_start, t_end, 1/fs)  # Time vector

    # Generate the signal
    x = np.array(case_rel_motion[beam_no]['hl_LOS_nom'])
    y = np.array(case_rel_motion[beam_no]['Mx_TB'])

    # # Compute FFT of the signal
    X = np.fft.fft(x)
    #Y = np.fft.fft(y)
    n = len(X)

    # Using function version:
    f_wsp, Sx_wsp = calculate_psd(t, x)
    f_Mx, Sx_Mx = calculate_psd(t, y)

    frequencies = np.linspace(0, fs/2, n//2)
    power_X = np.abs(X[:n // 2])**2
    power_X = power_X / n  # Normalized by Length
    
    # Find the maximum amplitud: 
    # Define the frequency range
    f_min = 0.2
    f_max = 0.5

    # Get the indices corresponding to the frequency range
    idx_range = np.where((frequencies >= f_min) & (frequencies <= f_max))

    # Find maximum frequency:
    max_freq = frequencies[idx_range][np.argmax(power_X[idx_range])]

    f_min = 0.3
    f_max = 0.7

    # Get the indices corresponding to the frequency range
    idx_range = np.where((frequencies >= f_min) & (frequencies <= f_max))

    # Extract the power values within the range and find the maximum
    #max_power = np.max(power_X[idx_range])
    three_P = frequencies[idx_range][np.argmax(power_X[idx_range])]

    # Plot the normalized PSD
    plt.figure(figsize=(12, 6))

    plt.axvline(x=max_freq, color='black', linestyle='--', linewidth=1.5)
    plt.axvline(x=three_P, color='black', linestyle='--', linewidth=1.5)
    
    plt.semilogy(f_Mx, Sx_Mx, label='Tower bottom fore-aft, MxTB')
    plt.semilogy(f_wsp, Sx_wsp, label='LOS wind speed weighted, Vy')
    
    plt.text(max_freq + 0.01, 1000*plt.gca().get_ylim()[0], '$\omega$T', color='black', fontsize=12, rotation='vertical')
    plt.text(three_P + 0.01, 1000*plt.gca().get_ylim()[0], '3P', color='black', fontsize=12, rotation='vertical')
    
    plt.title("PSD Tower bottom fore-aft (MxTB) and LOS weighted wind speed, Vy", fontsize=16, fontweight='bold')
    plt.xlabel("Frequency (Hz)", fontsize=12, fontweight='bold')
    plt.ylabel("PSD", fontsize=12, fontweight='bold')
    plt.legend(fontsize=12)
    plt.grid()
    plt.xlim([0, 1.5])

    if save:
        fname = 'comparison_psd_tower_lidar'
        path = '.\\figures\\06_relative_motion\\'
        func.check_path(path)
        
        plot_name = path + fname + '.png'
        plt.savefig(plot_name)
    
    plt.show()
        
    return 